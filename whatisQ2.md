
# BIRIYANI

## Infrastructure

### Robot Chefs

- From now on, there will be M Robot Chefs present in the kitchen preparing vessels of Biryani.
- Each Robot Chef can prepare a random number (greater than or equal to 1) of Biryani vessels at a time.
- Each Biryani vessel has a capacity to serve P Students.

### Serving Tables

- There will be N Serving Tables present in the mess.
- Each Serving Table has a Serving Container which loads a Biryani vessel prepared by any of our Robot Chefs. Only one vessel of Biryani can be loaded at a time into the Serving Container. The Serving Container can be refilled only when it is empty.
- The Serving Table incorporates the latest state of the art technologies and has automated the process of serving Biryani through multiple slots available on it.
- As long as a serving table has enough portions of Biryani left in the serving container, it makes a random number of slots available for the students to collect a portion of Biryani from the Serving Table. Note that the number of slots should always be less than or equal to the portions left in the Serving Container. Read down for exact limits.
- Thus there is a possibility that once a Serving Table has x available slots and later has greater than x or less than x slots available. After serving x portions (and consequently x students) the portions available in the Serving Container is updated.

### Student

- K Students have registered for the Biryani.

## Pipeline

### Robot Chef (P)

- Each Robot chef takes w seconds (random between 2-5) to prepare r vessels (random between 1-10) of biryani at any particular time. Each vessel has a capacity to feed p students (random between 25-50).
- Once the Robot Chef is done preparing the Biryani Vessels, he invokes the function biryani_ready() and does not return from it until all the biryani vessels he has cooked are loaded into any of the serving containers.
- Once all the biryani vessels are empty, the biryani_ready() function returns and the Robot Chef resumes making another batch of Biryani.

### Serving Tables (P)

- Each Serving table’s serving container is initially empty. It waits for any of the Robot Chefs to load a vessel of Biryani into the serving container. The serving table cannot go into serving mode as long as its container is empty.
- Once the serving container is full, it enters into the serving mode. It invokes a function ready_to_serve_table(int number_of_slots) in which number_of_slots (chosen randomly between 1-10) denotes the number of slots available at the particular serving table at that instant. The function must not return until either all the serving slots of the serving table are full or all the waiting students have been assigned a slot. Note that student can be assigned a slot at any of the serving tables. (If there is no waiting student, then the function returns)

### Students

- Once a student arrives in the mess, he/she invokes a function wait_for_slot(). This function does not return until a Serving table with a free slot is available, that is ready_to_serve_table method is in progress. Once the function returns the students goes to the allocated slot and waits for the slot to serve Biryani.
- Once the student is in the slot, he/she will call the function student_in_slot() to let the Serving Table know that he/she has arrived at the slot.
- Once all the students have been served you are done for the day.

## Purpose

To convince the Mess Committee you have decided to build a simulation of your efficient serving pipeline.

## Instructions

- Each Robot Chef, Serving Table and Student are threads.
- Stop the simulation when all the students are served.
- A Serving Table is used multiple times for serving. That means if there are still students left to be served, the serving table should invoke ready_to_serve_table().
- Use appropriate small delays for the simulation.
- Prepare a detailed report explaining your implementation and assumptions. Note that the report will be graded.
- The use of semaphores is not allowed. You can use only one mutex lock per Serving Table and Robot Chef.
- Your simulation should allow for multiple students to arrive at a Serving Table simultaneously. It must be possible for several students to have called wait_for_slot() function and the wait_for_slot function returned for each of the student before any of the student calling student_in_slot function
- Your simulation must not result in dead-locks.
- You are allowed to declare more functions if you require.

## Goal

The goal of this problem is for you to appreciate the use of threads and synchronisation techniques. There are no strict restrictions on the output format, as long as the simulation functions smoothly and displays appropriate messages.

## Input

M = Number of robot chefs
N = Number of serving containers/tables
P = Number of students served per container or slots per table
K = Number of students registered
w = Number of seconds taken by the chef to make r vessels
r = Number of vessels made by the chef at a time
x = Number of slots of biriyani available at the table

## PRODUCER CONSUMER

### Part 1

#### ROBOT CHEF (producer)

- produces r number of vessels that can serve p students in w seconds
- Once all r vessels are produced, invoke biriyani_ready() function that returns once all vessels are loaded
- once biriyani_ready() function returns, start producing again

#### Serving container (consumer)

- consumes one vessel at a time
- Refill if it is empty (no slots are available)

#### Commodity exchanged

- Vessel that has p slots capable of serving p students

### Part 2

#### Serving container and table (producer)

- Initially empty
- Waits for Biriyani vessel to be loaded into container
- Once full (i.e. has a vessel) go to SERVING MODE
- invoke a ready_to_serve_table(int number_of_slots) function with number_of_slots=random(1,10) that returns until all the serving slots (x is the current number of waiting slots) are over or all waiting students have been assigned a slot at any table or all students are done

- So there are two modes:

1. WAITING MODE
2. SERVING MODE

#### Student (consumer)

- Arrive at mess
- invoke wait_for_slot() function that returns once a Serving table with a free slot is available (ready_to_serve_table() method is still under execution)
- Once the function is returned, go to the allocated slot, call student_in_slot(), get the biriyani from table, reduce the amount of food
- Is served

## FUNCTIONS INVOLVED

### Robot chef_f

biriyani_ready()

### Serving container and table_f

ready_to_serve_table(int number_of_slots)

### Student_f

wait_for_slot()

## IMPLEMENTING WHAT

- First of all, I have no clue what to do. I initially thought of this as a producer-consumer problem, but it doesn't seem so.

### If this is a producer-consumer problem

#### Before that what is a producer-consumer problem

- Two condition variables control access to the buffer. One condition variable is used to tell if the buffer is full, and the other is used to tell if the buffer is empty.
- When the producer wants to add an item to the buffer, it checks to see if the buffer is full; if it is full the producer blocks on the cond_wait() call, waiting for an item to be removed from the buffer. When the consumer removes an item from the buffer, the buffer is no longer full, so the producer is awakened from the cond_wait() call. The producer is then allowed to add another item to the buffer.
- The consumer works, in many ways, the same as the producer. The consumer uses the other condition variable to determine if the buffer is empty. When the consumer wants to remove an item from the buffer, it checks to see if it is empty. If the buffer is empty, the consumer then blocks on the cond_wait() call, waiting for an item to be added to the buffer. When the producer adds an item to the buffer, the consumer's condition is satisfied, so it can then remove an item from the buffer.

#### Good, but why is this here tho

##### The condition variables involved can be

1. jobless_cook = No of jobless robot cooks waiting for a chance to cook (stuck on biriyani_ready() as there are no more free serving vessels to place them on)
2. free_vessel = No of free serving vessels
3. serving_per_vessel[no_of_vessel] = No of servings left on each vessel
4. free_slot_per_table[no_of_table] = No of free slots per table

#### WAIT NO SEMAPHORES ALLOWED

You can use only one mutex lock per Serving Table and Robot Chef

#### Now lets look at how to do things

#### lets look at the parameters

##### GLOBAL VARIABLES

max_number_of_slots_per_table
number_of_cooks
number_of_serving_tables
number_of_students
number_of_students_left_to_serve

##### ROBOT COOK

Each robot cook has an

- index
- a thread id
- mutex
- a conditioner (idk what it does tho)
- no of biriyani vessels left to serve
- no of vessels made at a time
- isbusy

##### SERVING TABLE

Each serving table has an

- index
- a thread id
- mutex
- number_of_slots_filled
- slot

##### SLOT

- index
- serving table
- student (if any)

##### STUDENT

Each student has an

- index
- a thread id
- current state
- slot

#### Algorithm

I dont know.. The algo  
chill

##### Initialisation

initialise the robot cook as follows:

- index = i
- mutex = NULL
- conditioner ?
- isbusy = 0

initialise the serving table as follows:

- index = i
- mutex = NULL
- conditioner ?
- number_of_slots_filled = 0

initialise the student:

- index = i
- current_state = NOT_ARRIVED

##### algorithm ? I don't know

start all student threads and join them  
start all cooks threads and join them  
start all serving table threads and join them  
Do BS
then free all memories (like how you do in life)

##### student process to do

```
student()
{
    printf("\n Student has arrived. now he needs food")
    current_state = WAITING
    printf("\n Student waiting for food")
    slot= wait_for_slot()
    current_state = GOT_A_SLOT
    printf("\n Student got a slot")
    student_in_slot()
    current_state = DONE
    printf("\n Student done with biriyani and is leaving")
}
```

##### cooks process to do

```
cook()
{
    while(number_of_students_left_to_serve!=0)
    {
        printf("\n Cook is jobless at the moment")
        isbusy=0
        printf("\n Cook is making biriyani")
        isbusy=1
        sleep(r)
        printf("\n Cook is done making it and is waiting for everything to be loaded")
        biriyani_ready()
        printf("\n All vessels made by the cook is now loaded. have fun!")
        isbusy=0
    }
}
```

##### serving table process to do

```
serving_table()
{
    while(number_of_students_left_to_serve!=0)
    {
        printf("\n Serving table empty at the moment")
    }
}
```

## REMEMBER MUTEXES ARE APPLIED FOR

The use of semaphores is not allowed. You can use only one mutex lock per Serving Table and Robot Chef.

- serving table status

- cooks status

I am sorry that students don't get mutexes  
So what about their status?
I DONT KNOW  
WAIT, CAN WE DO SOMETHING REGARDING TABLES?  
LIKE EACH TABLE HAVING SLOTS AND EACH SLOT HAS A STUDENT RIGHT?  
YEA THAT SEEMS OK

### Btw what exactly is a mutex? m

In computer science, a lock or mutex (from mutual exclusion) is a synchronization mechanism for enforcing limits on access to a resource in an environment where there are many threads of execution. A lock is designed to enforce a mutual exclusion concurrency control policy.(Wikipedia)

When I am having a big heated discussion at work, I use a rubber chicken which I keep in my desk for just such occasions. The person holding the chicken is the only person who is allowed to talk. If you don't hold the chicken you cannot speak. You can only indicate that you want the chicken and wait until you get it before you speak. Once you have finished speaking, you can hand the chicken back to the moderator who will hand it to the next person to speak. This ensures that people do not speak over each other, and also have their own space to talk.  
The chicken is the mutex. People hoilding the mu.. chicken are competing threads. The Moderator is the OS. When people requests the chicken, they do a lock request. When you call mutex.lock(), your thread stalls in lock() and makes a lock request to the OS. When the OS detects that the mutex was released from a thread, it merely gives it to you, and lock() returns - the mutex is now yours and only yours. Nobody else can steal it, because calling lock() will block him. There is also try_lock() that will block and return true when mutex is yours and immediately false if mutex is in use.  

Consider single toilet with a key. When someone enters, they take the key and the toilet is occupied. If someone else needs to use the toilet, they need to wait in a queue. When the person in the toilet is done, they pass the key to the next person in queue.

### SO MY PLAN

Use global variables for the number of free cooks and number of slots free  
Use a for loop to simulate the arrival of students  
This would mean a change in the main thing but yeah... idk  
IDK anything  
Ok  

#### EVM CODE what I inferred

In the EVM code, what they did is:  
in an infinite loop locked the mutex, then checked if all voters are finished and then unlocked it
if everyone is over break the loop  
otherwise, lock it, and then set the number of slots in it.  
unlock and then do another loop on the number of slots  
then check for a random voter if he is waiting, if so, assign him and finsh him off (all locked)
  
they did the condition mutex for the voting phase (analogous to getting biriyani) {can wait}

#### ANOTHER WAY

Part 1:  
BOOTH: cooks  
VOTER: tables  
EVM: vessels  
NUMBER OF SLOTS: vessels made by cook  

Part 2:
EVM: tables  
VOTER: students  
NUMBER OF SLOTS: slots made available on the table

##### NOPE TOO COMPLICATED
