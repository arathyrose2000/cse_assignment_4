#include "5_structure_defintions.h"

void *Rider_thread(void *arg);
int BookCab(Rider **);
void makePayment(Rider **Ri);
void Rider_create_thread(Rider *R);
void Server_create_thread(Server *S);
void Rider_join_thread(Rider R);

void *Server_thread(void *arg);
void Server_join_thread(Server S);