#include "4_global_variable_semaphores.h"

typedef struct Driver
{
    int in;
    int state;
    int type;
    int no_of_passengers;
    int Rider_in_cab[2];
} Driver;
typedef struct Rider
{
    int in;
    int state;
    int type;
    int arrivalTime;
    int maxWaitTime;
    int RideTime;
    int Driver_of_cab;
    int Server_to_pay;
} Rider;
typedef struct Server
{
    int in;
    int state;
    pthread_t tid;
    int paymentTime;
    int Rider_in_payment;
    pthread_mutex_t mutex;
} Server;

void Driver_init(Driver *D, int i)
{
    D->in = i;
    D->state = waitState;
    D->type = NO_RIDE;
    D->no_of_passengers = 0;
    D->Rider_in_cab[0] = D->Rider_in_cab[1] = -1;
}
void Rider_init(Rider *R, int i)
{
    R->in = i;
    R->state = free_state;
    R->type = random_integer(0, 1);
    R->maxWaitTime = random_integer(5, 10);
    R->RideTime = random_integer(10, 20);
    R->arrivalTime = random_unique_array[i]; // random_integer(0, 10); //TODO: set them as unique
    R->Driver_of_cab = -1;
    R->Server_to_pay = -1;
}
void Server_init(Server *S, int i)
{
    S->in = i;
    S->state = waiting;
    S->paymentTime = pT;
    S->Rider_in_payment = -1;
    pthread_mutex_init(&(S->mutex), NULL);
}