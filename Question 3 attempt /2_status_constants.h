#include "1_all_h.h"

//states of the driver
#define waitState 0      // no rider in cab​
#define onRidePremier 1  // Premier ride
#define onRidePoolFull 2 //​ pool ride with two riders
#define onRidePoolOne 3  // pool ride with only one rider

//states of the server
#define busy 1       //in some payment transaction
#define free_state 0 //completely free

//states of the rider
#define waiting 0 //waiting for a cab
#define inCab 1   //now in cab
#define wait_for_pay 2
#define in_payment 3
#define payment_done -1 //payment at server done

// cab types
#define PREMIER 1
#define POOL 0
#define NO_RIDE -1

//Constant time periods
#define pT  2