#include "3_cleanup.h"

int number_of_cabs;
int number_of_riders;
int number_of_servers;

sem_t number_of_cabs_free;
sem_t number_of_cabs_in_premier;
sem_t number_of_cabs_in_pool_one;
sem_t number_of_cabs_in_pool_two;
sem_t number_of_servers_free;
sem_t number_of_servers_occupied;

void semaphore_initialisation()
{
    sem_init(&number_of_cabs_free, 0, number_of_cabs);
    sem_init(&number_of_cabs_in_premier, 0, 0);
    sem_init(&number_of_cabs_in_pool_one, 0, 0);
    sem_init(&number_of_cabs_in_pool_two, 0, 0);
    sem_init(&number_of_servers_free, 0, number_of_servers);
    sem_init(&number_of_servers_occupied, 0, number_of_servers);
}

void get_values()
{
    printf("\nEnter the number of:\n");
    printf("\tcabs: ");
    scanf("%d", &number_of_cabs);
    printf("\tservers: ");
    scanf("%d", &number_of_servers);
    printf("\triders: ");
    scanf("%d", &number_of_riders);
}