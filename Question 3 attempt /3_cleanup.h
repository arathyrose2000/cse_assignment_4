#include "2_status_constants.h"
typedef sem_t Semaphore;
void *check_malloc(int size)
{
    void *p = malloc(size);
    if (p == NULL)
    {
        perror(" malloc failed ");
        exit(-1);
    }
    return p;
}
Semaphore *make_semaphore(int n)
{
    Semaphore *sem = (Semaphore *)check_malloc(sizeof(Semaphore));
    int ret = sem_init(sem, 0, n);
    if (ret == -1)
    {
        perror(" sem_init failed ");
        exit(-1);
    }
    return sem;
}
