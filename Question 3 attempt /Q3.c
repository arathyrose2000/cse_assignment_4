#define _POSIX_C_SOURCE 199309L
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>

//states of the driver
#define waitState 0      // no rider in cab​
#define onRidePremier 1  // Premier ride
#define onRidePoolFull 2 //​ pool ride with two riders
#define onRidePoolOne 3  // pool ride with only one rider
//states of the server
#define busy 1       //in some payment transaction
#define free_state 0 //completely free
//states of the rider
#define waiting 0 //waiting for a cab
#define inCab 1   //now in cab
#define wait_for_pay 2
#define in_payment 3
#define payment_done -1 //payment at server done
// cab types
#define PREMIER 1
#define POOL 0
//Constant time periods
#define paymentTime 2

//global arrays for the states
int driver_states[1000];
int server_states[1000];
int rider_states[1000];

//global array containing the list of all the thread IDs
pthread_t driver_thread[1000];
pthread_t server_thread[1000];
pthread_t rider_thread[1000];

//global variables for the number of drivers, servers and riders
int number_of_drivers;
int number_of_servers;
int number_of_riders;

//mutex1 involved
pthread_mutex_t mutex1;
pthread_mutex_t mutex2;

//get all details
void get_details()
{
    printf("\nEnter the number of:\n");
    printf("\tdrivers: ");
    scanf("%d", &number_of_drivers);
    printf("\tservers: ");
    scanf("%d", &number_of_servers);
    printf("\triders: ");
    scanf("%d", &number_of_riders);
}
//auxillary functions
long double get_time()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_nsec / (1e9) + ts.tv_sec;
}
//book a cab
void BookCab(int i, int type, int maxWaitTime, int RideTime)
{
    long double start_time = get_time(), end_time;
    int driven_by, flag = 0;
    maxWaitTime = maxWaitTime % 50;
    maxWaitTime = (maxWaitTime < 0) ? -maxWaitTime : maxWaitTime;
    RideTime = RideTime % 10;
    RideTime = (RideTime < 0) ? -RideTime : RideTime;
    while (rider_states[i] == waiting)
    {
        //check if the time has exceeded
        end_time = get_time();
        if (end_time - start_time > maxWaitTime)
        {
            printf("\nRider %d: Time Out. Hence exiting the system", i);
            return;
        }
        //otherwise lock the mutex1 and check if any cab is now free
        pthread_mutex_lock(&mutex1);
        if (type == PREMIER)
        {
            for (int j = 0; j < number_of_drivers; j++)
                if (driver_states[j] == waitState)
                {
                    printf("\nRider %d: Cab with driver %d found and is now on a premier ride", i, j);
                    driver_states[j] = onRidePremier;
                    rider_states[i] = inCab;
                    driven_by = j;
                    break;
                }
        }
        else if (type == POOL)
        {
            for (int j = 0; j < number_of_drivers; j++)
                if (driver_states[j] == waitState)
                {
                    printf("\nRider %d: Cab with driver %d found and is now on a pool ride alone", i, j);
                    driver_states[j] = onRidePoolOne;
                    rider_states[i] = inCab;
                    driven_by = j;
                    break;
                }
                else if (driver_states[j] == onRidePoolOne)
                {
                    printf("\nRider %d: Cab with driver %d found and is now on a full pool ride", i, j);
                    driver_states[j] = onRidePoolFull;
                    rider_states[i] = inCab;
                    driven_by = j; //flag=1;
                    break;
                }
        }
        pthread_mutex_unlock(&mutex1);
    }
    sleep(RideTime % 10);
    printf("\nRider %d: Done riding; now about to pay the bill", i);
    pthread_mutex_lock(&mutex1);
    driver_states[driven_by] = waitState;
    pthread_mutex_unlock(&mutex1);
    rider_states[i] = wait_for_pay;
    MakePayment(i);
}
//make payment
void MakePayment(int i)
{
    int payment_done_at;
    pthread_mutex_lock(&mutex2);
    while (rider_states[i] == wait_for_pay)
        for (int j = 0; j < number_of_servers; j++)
            if (server_states[j] == free_state)
            {
                printf("\nRider %d: A free server %d found and payment initiated", i, j);
                payment_done_at = j;
                server_states[j] = busy;
                rider_states[i] = in_payment;
                break;
            }
    pthread_mutex_unlock(&mutex2);
    sleep(paymentTime);
    pthread_mutex_lock(&mutex2);
    server_states[payment_done_at] = free_state;
    rider_states[i] = payment_done;
    pthread_mutex_unlock(&mutex2);
    printf("\nRider %d: Payment done and now he leaves the system", i);
}
struct rider_package
{
    int i, type, maxWaitTime, RideTime;
};
void *rider_thread_function(void *s)
{
    struct rider_package str = *(struct rider_package *)s;
    int i = str.i;
    printf("\nRider %d: Books a cab with the following details: ", i);
    printf("\tType: %s \tMax Wait Time: %d \tRide Time: %d", str.type == PREMIER ? "Premier" : "Pool", str.maxWaitTime, str.RideTime);
    BookCab(i, str.type, str.maxWaitTime, str.RideTime);
}
void *driver_thread_function(void *s)
{
    int i = *(int *)s;
    printf("\nDriver %d: ", i);
}
void *server_thread_function(void *s)
{
    int i = *(int *)s;
    printf("\nServer %d: ", i);
}
void init_thread_driver()
{
    for (int i = 0; i < number_of_drivers; i++)
    {
        driver_states[i] = waitState;
        pthread_create(&(driver_thread[i]), NULL, driver_thread_function, &i);
    }
}
void init_thread_server()
{
    for (int i = 0; i < number_of_servers; i++)
    {
        server_states[i] = free_state;
        pthread_create(&(server_thread[i]), NULL, server_thread_function, &i);
    }
}
void init_thread_rider()
{
    for (int i = 0; i < number_of_riders; i++)
    {
        struct rider_package r;
        r.i = i;
        r.type = (rand() % 2);
        r.maxWaitTime = (rand() % (10 - 5 + 1)) + 5;
        r.RideTime = (rand() % (40 - 5 + 1)) + 5;
        rider_states[i] = waiting;
        pthread_create(&(rider_thread[i]), NULL, rider_thread_function, &r);
    }
}
void join_thread_driver()
{
    for (int i = 0; i < number_of_drivers; i++)
        pthread_join((driver_thread[i]), 0);
}
void join_thread_rider()
{
    for (int i = 0; i < number_of_riders; i++)
        pthread_join((rider_thread[i]), 0);
}
void join_thread_server()
{
    for (int i = 0; i < number_of_servers; i++)
        pthread_join((server_thread[i]), 0);
}
int main()
{
    srand(time(0));
    get_details();
    pthread_mutex_init(&mutex1, NULL);
    pthread_mutex_init(&mutex2, NULL);
    // init_thread_driver();
    // init_thread_server();
    init_thread_rider();
    //  join_thread_driver();
    //  join_thread_server();
    join_thread_rider();
    pthread_mutex_destroy(&mutex1);
    pthread_mutex_destroy(&mutex2);
    return 0;
}