#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>
#include <pthread.h>
#include <semaphore.h>
#define _POSIX_C_SOURCE 199309L

//auxillary functions
long double get_time()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_nsec / (1e9) + ts.tv_sec;
}
int random_integer(int minimum, int maximum)
{
    return random() % (maximum - minimum + 1) + minimum; //inclusivedddx
}
void sleep_random(int minimum_time_to_sleep, int maximum_time_to_sleep)
{
    sleep(random_integer(0, maximum_time_to_sleep));
}
int random_unique_array[1000];
int set_randomized(int n, int maximum)
{
    int ispresent[maximum];
    for (int i = 0; i < maximum; i++)
        ispresent[i] = 0;
    for (int i = 0; i < n; i++)
    {
        int ok = random_integer(0, maximum);
        while (ispresent[ok] != 0)
            ok = random_integer(0, maximum);
        random_unique_array[i] = ok;
        ispresent[ok] = 1;
    }
}