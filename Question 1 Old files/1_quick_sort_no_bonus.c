
#include "all_h.h"

// TODO: COMMON FUNCTIONS

/**
 * swapping two integers
 * */
void swap(int *a, int *b)
{
    int t = *a;
    *a = *b;
    *b = t;
}

/**
 * This function below returns the array arr partitioned around the pivot
 * Pivot location in the final array is given by i+1
 * */
int partition(int *arr, int low, int high)
{
    int pivot = arr[high];
    int i = (low - 1);
    for (int j = low; j <= high - 1; j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

//TODO: CONCURRENT QUICK SORT

/**
 * Allocating the shared memory
 * */
int *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (int *)shmat(shm_id, NULL, 0);
}

/**
 * Creates two separate processes responsible for carrying out the left and right quicl sorts and then merge them
 * */
void concurrent_quickSort(int *arr, int low, int high)
{
    if (low > high)
        _exit(1);
    int pi = partition(arr, low, high);
    int pid1 = fork();
    if (pid1 == 0)
    {
        concurrent_quickSort(arr, low, pi - 1); //sort the lower part
        _exit(1);
    }
    else
    {
        int pid2 = fork();
        if (pid2 == 0)
        {
            concurrent_quickSort(arr, pi + 1, high); //sort the upper part
            _exit(1);
        }
        else
        {
            int status; //wait for both of them to finish
            waitpid(pid1, &status, 0);
            waitpid(pid2, &status, 0);
        }
    }
}

void run_concurrent_quicksort(long long int n, long long int a[])
{
    int *arr = shareMem(sizeof(int) * (n + 1));
    for (int i = 0; i < n; i++)
        *(arr + i) = a[i];
    concurrent_quickSort(arr, 0, n - 1);
    for (int i = 0; i < n; i++)
        printf("%d ", *(arr + i));
    shmdt(arr);
    return;
}

//TODO: THE MAIN THING

int main()
{
    long long int n;
    scanf("%lld", &n);
    long long int a[10000];
    for (int i = 0; i < n; i++)
        scanf("%lld", &a[i]);
    printf("Running concurrent_mergesort for n = %lld\n", n);
    run_concurrent_quicksort(n, a);
    return 0;
}