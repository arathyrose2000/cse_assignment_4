#define _POSIX_C_SOURCE 199309L // required for clock
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>

// TODO: COMMON FUNCTIONS

/**
 * swapping two long long integers
 * */

void swap(long long int *a, long long int *b)
{
    long long int t = *a;
    *a = *b;
    *b = t;
}

/**
 * This function below returns the array arr partitioned around the pivot
 * Pivot location in the final array is given by i+1
 * */
long long int partition(long long int *arr, long long int low, long long int high)
{
    long long int element_pivot = rand() % (high - low + 1) + low;
    long long int pivot = arr[high];
    long long int i = (low - 1);
    for (long long int j = low; j <= high - 1; j++)
    {
        if (arr[j] < pivot)
        {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

// TODO: Normal quick sort

void normal_quicksort(long long int *arr, long long int low, long long int high)
{
    if (low < high)
    {
        long long int pi = partition(arr, low, high);
        normal_quicksort(arr, low, pi - 1);
        normal_quicksort(arr, pi + 1, high);
    }
}

void run_normal_quicksort(long long int n, long long int a[])
{
    printf("\nRunning normal quicksort...");
    normal_quicksort(a, 0, n - 1);
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", a[i]);
}

// TODO: Concurrent quick sort using processes

/**
 * Allocating the shared memory
 * */
long long int *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    long long int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (long long int *)shmat(shm_id, NULL, 0);
}

/**
 * Creates two separate processes responsible for carrying out the left and right quicksorts and then merge them
 * */

void concurrent_process_quicksort(long long int *arr, long long int low, long long int high)
{
    if (low > high)
        _exit(1);
    long long int pi = partition(arr, low, high);
    long long int pid1 = fork();
    if (pid1 == 0)
    {
        concurrent_process_quicksort(arr, low, pi - 1); // sort the lower part
        _exit(1);
    }
    else
    {
        long long int pid2 = fork();
        if (pid2 == 0)
        {
            concurrent_process_quicksort(arr, pi + 1, high); // sort the upper part
            _exit(1);
        }
        else
        {
            signed status; // wait for both of them to finish
            waitpid(pid1, &status, 0);
            waitpid(pid2, &status, 0);
        }
    }
}

void run_concurrent_process_quicksort(long long int n, long long int a[])
{
    printf("\nRunning concurrent quicksort using processes...");
    long long int *arr = shareMem(sizeof(long long int) * (n + 1));
    for (long long int i = 0; i < n; i++)
        *(arr + i) = a[i];
    concurrent_process_quicksort(arr, 0, n - 1);
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", *(arr + i));
    shmdt(arr);
}

// TODO: Concurrent quick sort using threads

struct packet
{
    long long int l;
    long long int r;
    long long int *arr;
};

void *concurrent_threaded_quicksort(void *a)
{
    struct packet *args = (struct packet *)a;
    long long int low = args->l;
    long long int high = args->r;
    long long int *arr = args->arr;
    if (low > high)
        return NULL;
    // partition the array
    long long int pi = partition(arr, low, high);
    // sort lower array:  normal_quicksort(arr, low, pi - 1);
    struct packet a1;
    a1.l = low;
    a1.r = pi - 1;
    a1.arr = arr;
    pthread_t tid1;
    pthread_create(&tid1, NULL, concurrent_threaded_quicksort, &a1);
    // sort higher array:    normal_quicksort(arr, pi + 1, high);
    struct packet a2;
    a2.l = pi + 1;
    a2.r = high;
    a2.arr = arr;
    pthread_t tid2;
    pthread_create(&tid2, NULL, concurrent_threaded_quicksort, &a2);
    // wait for the two parts to get sorted
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
}

void run_concurrent_thread_quicksort(long long int n, long long int a[])
{
    printf("\nRunning concurrent quicksort using threads...");
    pthread_t tid;
    struct packet p;
    p.l = 0;
    p.r = n - 1;
    p.arr = a;
    pthread_create(&tid, NULL, concurrent_threaded_quicksort, &p);
    pthread_join(tid, NULL);
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", *(p.arr + i));
}

// TODO: MAIN THING

/**
 * Gets the current time of the clock in the system
 * */
long double get_time()
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_nsec / (1e9) + ts.tv_sec;
}

int main()
{
    srand(time(0));
    long long int n;
    scanf("%lld", &n);
    long long int a[10000];
    for (long long int i = 0; i < n; i++)
        scanf("%lld", &a[i]);
    long double start_time, end_time, t1, t2, t3;
    start_time = get_time();
    run_concurrent_process_quicksort(n, a);
    end_time = get_time();
    t1 = end_time - start_time;
    printf("Time: %Lf", t1);
    start_time = get_time();
    run_concurrent_thread_quicksort(n, a);
    end_time = get_time();
    t2 = end_time - start_time;
    printf("Time: %Lf", t2);
    start_time = get_time();
    run_normal_quicksort(n, a);
    end_time = get_time();
    t3 = end_time - start_time;
    printf("Time: %Lf", t3);
    printf("\nNormal_quicksort ran");
    printf("\n\t --- %Lf times faster than concurrent_process_quicksort", t1 / t3);
    printf("\n\t --- %Lf times faster than concurrent_thread_quicksort", t2 / t3);
}
