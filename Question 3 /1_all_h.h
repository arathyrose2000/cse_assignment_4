// contains all the header files used in this code

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>
#include <pthread.h>
#define _POSIX_C_SOURCE 199309L

#define is_debug 0 // change this variable to one to see the final conditions of each entity