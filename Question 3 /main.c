// the main function

#include "9_drivers.h"

int main()
{
    srand(time(0));
    get_details();
    set_randomized(number_of_riders, (int)(1.5 * number_of_riders));
    //allocate memory
    drivers = (Driver **)malloc(number_of_drivers * sizeof(Driver *));
    riders = (Rider **)malloc(number_of_riders * sizeof(Rider *));
    servers = (Server **)malloc(number_of_servers * sizeof(Server *));
    //initialise riders
    for (int i = 0; i < number_of_riders; i++)
    {
        riders[i] = (Rider *)malloc(sizeof(Rider));
        Rider_init(riders[i], i);
        Rider_debug_print(*riders[i]);
    }
    //initialise drivers
    for (int i = 0; i < number_of_drivers; i++)
    {
        drivers[i] = (Driver *)malloc(sizeof(Driver));
        Driver_init(drivers[i], i);
        Driver_debug_print(*drivers[i]);
    }
    //initialise servers
    for (int i = 0; i < number_of_servers; i++)
    {
        servers[i] = (Server *)malloc(sizeof(Server));
        Server_init(servers[i], i);
        Server_debug_print(*servers[i]);
    }
    // create threads
    for (int i = 0; i < number_of_riders; i++)
        Rider_create_thread(riders[i]);
    for (int i = 0; i < number_of_riders; i++)
        Rider_join_thread(*riders[i]);
    //free memory
    for (int i = 0; i < number_of_riders; i++)
    {
        Rider_debug_print(*riders[i]);
        free(riders[i]);
    }
    for (int i = 0; i < number_of_drivers; i++)
    {
        Driver_debug_print(*drivers[i]);
        pthread_mutex_destroy(&drivers[i]->mutex);
        free(drivers[i]);
    }
    for (int i = 0; i < number_of_servers; i++)
    {
        Server_debug_print(*servers[i]);
        pthread_mutex_destroy(&servers[i]->mutex);
        free(servers[i]);
    }
    free(riders);
    free(servers);
    free(drivers);
    return 0;
}