// contains all the secondary functions used in this code

#include "2_all_constants.h"

long double get_time() // gets the current time of the system
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_nsec / (1e9) + ts.tv_sec;
}

int random_integer(int minimum, int maximum) // generate a random number between minimum and maximum (both inclusive)
{
    return random() % (maximum - minimum + 1) + minimum; //inclusivedddx
}

int random_unique_array[1000]; // to store the arrival time of the riders

int set_randomized(int n, int maximum) // create an array with n elements and no numbers repeated but all random numbers between 0 and maxnum
{
    int ispresent[maximum];
    for (int i = 0; i < maximum; i++)
        ispresent[i] = 0;
    for (int i = 0; i < n; i++)
    {
        int ok = random_integer(0, maximum);
        while (ispresent[ok] != 0)
            ok = random_integer(0, maximum);
        random_unique_array[i] = ok;
        ispresent[ok] = 1;
    }
}