#include "6_threads_related_prototype.h"

void Rider_create_thread(Rider *R)
{
    pthread_create(&(R->tid), NULL, Rider_thread, (void *)R);
}

void Rider_join_thread(Rider R)
{
    pthread_join(R.tid, NULL);
}

void *Rider_thread(void *arg)
{
    Rider *R = (Rider *)arg;
    Rider_debug_print(*R);
    sleep(R->arrivalTime);
    printf("\nRider %d: arrived", R->in);
    int status = BookCab(&R);
    if (status == -1)
    {
        Rider_debug_print(*R);
        R->state = payment_done;
        return NULL;
    }
    makePayment(&R);
    return NULL;
}

int BookCab(Rider **Ri)
{
    Rider *R = *Ri;
    long double start_time = get_time(), end_time;
    int flag = 0;
    while (flag == 0)
    {
        end_time = get_time();
        if (end_time - start_time > R->maxWaitTime)
        {
            printf("\nRider %d: Time Out. Hence exiting the system", R->in);
            R->state = payment_done;
            Ri = &R;
            return -1;
        }
        //get a free driver
        for (int i = 0; i < number_of_drivers; i++)
        {
            end_time = get_time();
            if (end_time - start_time > R->maxWaitTime)
            {
                printf("\nRider %d: Time Out. Hence exiting the system", R->in);
                R->state = payment_done;
                Ri = &R;
                return -1;
            }
            //now check the driver status
            pthread_mutex_lock(&(drivers[i]->mutex));
            if (drivers[i]->state == waitState)
            {
                printf("\nRider %d: Found driver %d", R->in, i);
                AcceptRide();
                drivers[i]->state = (R->type == PREMIER) ? onRidePremier : onRidePoolOne;
                drivers[i]->type = R->type;
                drivers[i]->no_of_passengers = 1;
                drivers[i]->Rider_in_cab[0] = R->in;
                R->state = inCab;
                R->Driver_of_cab = i;
                pthread_mutex_unlock(&(drivers[i]->mutex));
                flag = 1;
                break;
            }
            else if (drivers[i]->state == onRidePoolOne && R->type == POOL)
            {
                printf("\nRider %d: Found driver %d", R->in, i);
                AcceptRide();
                drivers[i]->state = onRidePoolFull;
                drivers[i]->no_of_passengers = 2;
                drivers[i]->Rider_in_cab[1] = R->in;
                R->state = inCab;
                R->Driver_of_cab = i;
                pthread_mutex_unlock(&(drivers[i]->mutex));
                flag = 1;
                break;
            }
            pthread_mutex_unlock(&(drivers[i]->mutex));
        }
    }
    Ri = &R;
    OnRide(R->RideTime);
    printf("\nRider %d: Ride over (%d) ", R->in, R->Driver_of_cab);
    EndRide();
    int cab_driver_id = R->Driver_of_cab;
    pthread_mutex_lock(&(drivers[cab_driver_id]->mutex));
    if (R->type == PREMIER)
    {
        drivers[cab_driver_id]->state = waitState;
        drivers[cab_driver_id]->type = NO_RIDE;
        drivers[cab_driver_id]->no_of_passengers = 0;
        drivers[cab_driver_id]->Rider_in_cab[0] = -1;
        R->state = wait_for_pay;
        R->Driver_of_cab = -1;
    }
    else if (R->type == POOL)
    {
        R->state = wait_for_pay;
        R->Driver_of_cab = -1;
        if (drivers[cab_driver_id]->state == onRidePoolOne)
        {
            drivers[cab_driver_id]->state = waitState;
            drivers[cab_driver_id]->type = NO_RIDE;
            drivers[cab_driver_id]->no_of_passengers = 0;
            drivers[cab_driver_id]->Rider_in_cab[0] = -1;
        }
        else if (drivers[cab_driver_id]->state == onRidePoolFull)
        {
            drivers[cab_driver_id]->state = onRidePoolOne;
            drivers[cab_driver_id]->type = POOL;
            drivers[cab_driver_id]->no_of_passengers = 1;
            int other_passenger = (drivers[cab_driver_id]->Rider_in_cab[0] == R->in) ? drivers[cab_driver_id]->Rider_in_cab[1] : drivers[cab_driver_id]->Rider_in_cab[0];
            drivers[cab_driver_id]->Rider_in_cab[0] = other_passenger;
            drivers[cab_driver_id]->Rider_in_cab[1] = -1;
        }
    }
    pthread_mutex_unlock(&(drivers[cab_driver_id]->mutex));
    Ri = &R;
    return 1;
}

void makePayment(Rider **Ri)
{
    Rider *R = *Ri;
    int flag = 0;
    while (flag == 0)
    {
        for (int i = 0; i < number_of_servers; i++)
        {
            //now check the server status
            pthread_mutex_lock(&(servers[i]->mutex));
            if (servers[i]->state == free_state)
            {
                printf("\nRider %d: Found server %d", R->in, i);
                servers[i]->state = busy;
                servers[i]->Rider_in_payment = R->in;
                R->state = in_payment;
                R->Server_to_pay = i;
                pthread_mutex_unlock(&(servers[i]->mutex));
                flag = 1;
                break;
            }
            pthread_mutex_unlock(&(servers[i]->mutex));
        }
    }
    Ri = &R;
    accept_payment();
    printf("\nRider %d: Payment_done (%d)", R->in, R->Server_to_pay);
    int pay_server_id = R->Server_to_pay;
    pthread_mutex_lock(&(servers[pay_server_id]->mutex));
    servers[pay_server_id]->state = free_state;
    servers[pay_server_id]->Rider_in_payment = -1;
    R->state = payment_done;
    R->Server_to_pay = -1;
    pthread_mutex_unlock(&(servers[pay_server_id]->mutex));
    Ri = &R;
    return;
}