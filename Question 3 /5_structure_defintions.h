//contains the structure definitions for the different entities used

#include "4_global_variables.h"

typedef struct Driver
{
    int in;
    int state;
    int type;
    int no_of_passengers;
    int Rider_in_cab[2];
    pthread_mutex_t mutex;
} Driver;

typedef struct Rider
{
    int in;
    int state;
    pthread_t tid;
    int type;
    int arrivalTime;
    int maxWaitTime;
    int RideTime;
    int Driver_of_cab;
    int Server_to_pay;
} Rider;

typedef struct Server
{
    int in;
    int state;
    pthread_t tid;
    int paymentTime;
    int Rider_in_payment;
    pthread_mutex_t mutex;
} Server;

void Driver_init(Driver *D, int i)
{
    D->in = i;
    D->state = waitState;
    D->type = NO_RIDE;
    D->no_of_passengers = 0;
    D->Rider_in_cab[0] = D->Rider_in_cab[1] = -1;
    pthread_mutex_init(&(D->mutex), NULL);
}

void Rider_init(Rider *R, int i)
{
    R->in = i;
    R->state = free_state;
    R->type = random_integer(0, 1);
    R->maxWaitTime = random_integer(5, 10);
    R->RideTime = random_integer(10, 20);
    R->arrivalTime = random_unique_array[i]; 
    R->Driver_of_cab = -1;
    R->Server_to_pay = -1;
}

void Server_init(Server *S, int i)
{
    S->in = i;
    S->state = waiting;
    S->paymentTime = pT;
    S->Rider_in_payment = -1;
    pthread_mutex_init(&(S->mutex), NULL);
}

void Rider_debug_print(Rider R)
{
    if (is_debug)
        printf("\nRider %d:  State=%d   type=%d   maxWaitTime=%d   RideTime=%d   Driver_of_cab=%d   Server_to_pay=%d   Arrival time=%d ", R.in, R.state, R.type, R.maxWaitTime, R.RideTime, R.Driver_of_cab, R.Server_to_pay, R.arrivalTime);
}

void Server_debug_print(Server S)
{
    if (is_debug)
        printf("\nServer %d:  State=%d   paymentTime=%d   Rider_in_payment=%d", S.in, S.state, S.paymentTime, S.Rider_in_payment);
}

void Driver_debug_print(Driver D)
{
    if (is_debug)
        printf("\nDriver %d:  State=%d   type=%d   no_of_passengers=%d   Rider_in_cab=%d %d", D.in, D.state, D.type, D.no_of_passengers, D.Rider_in_cab[0], D.Rider_in_cab[1]);
}

Driver **drivers;
Rider **riders;
Server **servers;