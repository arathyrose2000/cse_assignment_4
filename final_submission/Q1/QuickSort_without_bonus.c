// Deals with quick sort without bonus (refer readme for more)

#include "Concurrent_process_quick_sort.h"

int main()
{
    long long int n;
    long long int a[10000];
    printf("\nEnter the number of elements: ");
    scanf("%lld", &n);
    printf("\nEnter the array elements: ");
    for (int i = 0; i < n; i++)
        scanf("%lld", &a[i]);
    printf("Running concurrent_mergesort for n = %lld\n", n);
    run_concurrent_process_quicksort(n, a);
    return 0;
}