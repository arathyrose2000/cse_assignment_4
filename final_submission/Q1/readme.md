
# QUESTION 1 : Concurrent Quicksort

## AIM

Implement a Concurrent version of Quicksort algorithm

## INPUT

You are given the number of elements in the array N, and the elements of the array.

## OUTPUT

Sort the numbers of the array using a Concurrent version of Quicksort algorithm.

## LOGIC

In quick sort, these are steps:

1. Choose a pivot (a random element)
2. Partition the array around the pivot
3. Sort recursively for the two subarrays

So the steps done are as follows:

- When the number of elements in the array for a process is less than 5, perform an insertion sort to sort the elements of that array.
- Otherwise, partition the array around the pivot such that all the elements with a value less than the pivot are positioned before it, while all the elements with a value greater than the pivot are positioned after. In case of equality, they can go on either side of the partition.
- Recurse for the low and high subarray.

The recursion for the low and high subarray is done differently in each case.

- In case of concurrent quick sort with processes, the sorting is done parallely using processes.
- In case of concurrent quick sort with threads, the sorting is done parallely using threads.
- In case of normal quick sort, first the left subarray is sorted and then the right subarray is sorted.

## IMPLEMENTATION

### Common Functions- File to refer: Quick_sort.h

#### long double get_time()

Gets the current time of the clock in the system

#### void swap(long long int *a, long long int *b)

Swap the value of two variables a and b

#### long long int number_of_elements(long long int left, long long int right)

Return the number of elements in the subarray from left to right

#### long long int partition(long long int *arr, long long int low, long long int high)

Partition the array arr around a pivot chosen randomly and returns the pivot location in the final array.

#### void insertion_sort(long long int *arr, long long int low, long long int high)

Sorts the subarray of arr between low and high

### Concurrent quick sort using processes- File to refer: Concurrent_process_quick_sort.h

As usual, the size of the subarray is first checked. If it is less than 5, then insertion sort is done. Otherwise, partition the array using the partition() function to get higher and lower subarrays. Create a child process (using fork) that would sort the higher subarray by calling the same function again. The parent process would create another child process (using fork) that would run parallely to the first function made and sort the lower subarray by calling the same function again. The parent process waits patiently till both the children are done with their sorting process.

- For the non-bonus part, compile the file QuickSort_without_bonus.c and run it. It would sort the array using concurrent processes only

### Concurrent quick sort using threads- File to refer: Concurrent_threaded_quick_sort.h

As usual, the size of the subarray is first checked. If it is less than 5, then insertion sort is done. Otherwise, partition the array using the partition() function to get higher and lower subarrays. Create two threads using the pthread_create function, passing the array, the left and right of the two subarrays, and join them using the pthread_join function. Here the parent thread would wait till both the threads are over, i.e. finish sorting their subarrays.

### Normal quick sort- File to refer: Normal_quick_sort.h

Here, the recursion part is done using functions. As usual, the size of the subarray is first checked. If it is less than 5, then insertion sort is done. Otherwise, partition the array using the partition() function to get higher and lower subarrays. Then recursively call the same function to sort the upper and lower parts.

## COMPARISON OF THE PERFORMANCES

For comparing the performances, we use the get_time() function that gets the current time of the running process. Calculate the time before and after running the sorting for the same array, to get the time taken for each type of sorting procedure. Divide the time taken to get a performance ratio of the sort relative to others.

## SAMPLE RUN

To run the files run
```
gcc -pthread Q1.c  
./a.out
```

### SAMPLE INPUT

Enter the number of elements: 19

Enter the array elements:  5 2 6 3 4 7 5 9 21 5 2 4 1 9 0 3 4 3 53 53 2 42 24 23 6 2 32 3

### SAMPLE OUTPUT

Running concurrent quicksort using processes...
Resulting array: 0 1 2 2 3 3 3 4 4 4 5 5 5 6 7 9 9 21 53 Time: 0.001437
Running concurrent quicksort using threads...
Resulting array: 0 1 2 2 3 3 3 4 4 4 5 5 5 6 7 9 9 21 53 Time: 0.000708
Running normal quicksort...
Resulting array: 0 1 2 2 3 3 3 4 4 4 5 5 5 6 7 9 9 21 53 Time: 0.000007
Normal_quicksort ran
         --- 218.161631 times faster than concurrent_process_quicksort
         --- 107.524559 times faster than concurrent_thread_quicksort

### Comparison of performances

Here, we observe that the normal quicksort performed much much better than concurrent models of quick sort even though it runs the sorts sequentially. This is because of the huge overheads associated with the creation of threads and processes. Since threads are more lightweight than processes, we see that concurrent_thread_quicksort is faster than concurrent_process_quicksort.  
Therefore for small n, sort implementations in the increasing order of time taken for completion: normal_quicksort < concurrent_thread_quicksort < concurrent_process_quicksort  
For sufficiently large values of n, the overhead can be ignored. Hence, concurrent versions of quicksort performs better than the normal sequential one.  

## NOTE: The code might run into a segmentation fault when run for large values of n depending on the thread allocation and so on  

For function descriptions, refer the code
