// This file contains everything regarding normal quick sort

#include "Concurrent_threaded_quick_sort.h"

void normal_quicksort(long long int *arr, long long int low, long long int high) // use recursive fuctions that sort the left and right subarrays sequentially
{
    if (number_of_elements(low, high) <= 0)
        return;
    if (number_of_elements(low, high) < 5)
    {
        insertion_sort(arr, low, high);
        return;
    }
    long long int pi = partition(arr, low, high);
    normal_quicksort(arr, low, pi - 1);
    normal_quicksort(arr, pi + 1, high);
}

long double run_normal_quicksort(long long int n, long long int a[])
{
    long double start_time, end_time;
    printf("\nRunning normal quicksort...");
    start_time = get_time();
    normal_quicksort(a, 0, n - 1);
    end_time = get_time();
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", a[i]);
    printf("Time: %Lf", end_time - start_time);
    return end_time - start_time;
}