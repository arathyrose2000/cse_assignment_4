// This file contains all the header files common functions required to run the quick_sort program

#define _POSIX_C_SOURCE 199309L // required for clock
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <limits.h>
#include <fcntl.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <inttypes.h>
#include <math.h>

long double get_time() // Gets the current time of the clock in the system
{
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC_RAW, &ts);
    return ts.tv_nsec / (1e9) + ts.tv_sec;
}

void swap(long long int *a, long long int *b) // swap two variables
{
    long long int t;
    t = *a;
    *a = *b;
    *b = t;
}

long long int number_of_elements(long long int left, long long int right) // return the number of elements in the subarray from left to right
{
    return right - left + 1;
}

long long int partition(long long int *arr, long long int low, long long int high) // partition the array arr around a pivot chosen randomly and returns the pivot location in the final array.
{
    long long int pivot_location = rand() % (high - low + 1) + low;
    swap(&arr[high], &arr[pivot_location]);
    long long int pivot = arr[high];
    pivot_location = low - 1;
    for (long long int j = low; j <= high - 1; j++)
    {
        if (arr[j] < pivot)
        {
            pivot_location++;
            swap(&arr[pivot_location], &arr[j]);
        }
    }
    swap(&arr[pivot_location + 1], &arr[high]);
    return pivot_location + 1;
}

void insertion_sort(long long int *arr, long long int low, long long int high) // sorts the subarray of arr between low and high
{
    for (int i = low + 1; i <= high; i++)
    {
        long long int key = arr[i], j = i - 1;
        while (j >= low && arr[j] > key)
        {
            arr[j + 1] = arr[j];
            j--;
        }
        arr[j + 1] = key;
    }
}