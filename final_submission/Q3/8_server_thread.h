// contains all the functions related to servers

#include "7_rider_thread.h"

void Server_create_thread(Server *S)
{
    pthread_create(&(S->tid), NULL, Server_thread, (void *)S);
}

void Server_join_thread(Server S)
{
    pthread_join(S.tid, NULL);
}

void *Server_thread(void *arg)
{
    Server *S = (Server *)arg;
}

void accept_payment(Rider *R, int i)
{
    Server_debug_print(*servers[i]);
    Rider_debug_print(*R);
    servers[i]->state = busy;
    servers[i]->Rider_in_payment = R->in;
    Server_debug_print(*servers[i]);
    R->state = in_payment;
    Rider_debug_print(*R);
    R->Server_to_pay = i;
}

void do_payment(Rider *R)
{
    sleep(pT);
}

void payment_over(Rider *R, int pay_server_id)
{
    Server_debug_print(*servers[pay_server_id]);
    servers[pay_server_id]->state = free_state;
    Rider_debug_print(*R);
    servers[pay_server_id]->Rider_in_payment = -1;
    Server_debug_print(*servers[pay_server_id]);
    R->state = payment_done;
    R->Server_to_pay = -1;
    Rider_debug_print(*R);
}