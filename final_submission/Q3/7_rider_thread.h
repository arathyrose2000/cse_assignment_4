// contains all the functions related to riders

#include "6_threads_related_prototype.h"

void Rider_create_thread(Rider *R)
{
    pthread_create(&(R->tid), NULL, Rider_thread, (void *)R);
}

void Rider_join_thread(Rider R)
{
    pthread_join(R.tid, NULL);
}

void *Rider_thread(void *arg)
{
    Rider *R = (Rider *)arg;
    Rider_debug_print(*R);
    sleep(R->arrivalTime);
    printf("\nRider %d has arrived requiring a cab of type %s, ride time %d, and maximum wait time %d", R->in, (R->type == PREMIER) ? "PREMIER" : "POOL", R->RideTime, R->maxWaitTime);
    int status = BookCab(&R);
    if (status == -1)
    {
        Rider_debug_print(*R);
        R->state = payment_done;
        return NULL;
    }
    Rider_debug_print(*R);
    printf("\nRider %d has completed his ride and is now waiting for payment", R->in);
    makePayment(&R);
    Rider_debug_print(*R);
    printf("\nRider %d has left the system", R->in);
    return NULL;
}

int BookCab(Rider **Ri)
{
    Rider *R = *Ri;
    long double start_time = get_time(), end_time;
    int flag = 0;
    while (flag == 0)
    {
        end_time = get_time();
        if (end_time - start_time > R->maxWaitTime)
        {
            printf("\nRider %d had a time Out. Hence he is exiting the system", R->in);
            R->state = payment_done;
            Ri = &R;
            return -1;
        }
        //get a free driver
        for (int i = 0; i < number_of_drivers; i++)
        {
            if (get_time() - start_time > R->maxWaitTime)
            {
                printf("\nRider %d had a time Out. Hence he is exiting the system", R->in);
                R->state = payment_done;
                Ri = &R;
                return -1;
            }
            //now check the driver status
            pthread_mutex_lock(&(drivers[i]->mutex));
            if (drivers[i]->state == waitState)
            {
                AcceptRide(1, R, i);
                printf("\nDriver %d accepted the %d rider for a %s ride", i, R->in, (R->type == POOL) ? "pool" : "premier");
                pthread_mutex_unlock(&(drivers[i]->mutex));
                flag = 1;
                break;
            }
            else if (drivers[i]->state == onRidePoolOne && R->type == POOL)
            {
                printf("\nDriver %d accepted the %d rider for a %s ride with ride time %d", i, R->in, (R->type == POOL) ? "pool" : "premier", R->RideTime);
                AcceptRide(2, R, i);
                pthread_mutex_unlock(&(drivers[i]->mutex));
                flag = 1;
                break;
            }
            pthread_mutex_unlock(&(drivers[i]->mutex));
        }
    }
    Ri = &R;
    OnRide(R->RideTime);
    int cab_driver_id = R->Driver_of_cab;
    if (!drivers[cab_driver_id])
        perror("ERROR");
    pthread_mutex_lock(&(drivers[cab_driver_id]->mutex));
    if (!drivers[cab_driver_id])
        perror("ERROR");
    EndRide(&R, cab_driver_id);
    //  if (is_debug)
    if (!drivers[cab_driver_id])
        perror("ERROR");
    pthread_mutex_unlock(&(drivers[cab_driver_id]->mutex));
    printf("\nDriver %d has completed his ride with %d rider", cab_driver_id, R->in);
    Ri = &R;
    return 1;
}

void makePayment(Rider **Ri)
{
    Rider *R = *Ri;
    int flag = 0;
    while (flag == 0)
    {
        for (int i = 0; i < number_of_servers; i++)
        {
            //now check the server status
            pthread_mutex_lock(&(servers[i]->mutex));
            if (servers[i]->state == free_state)
            {
                printf("\nRider %d has found a free server %d", R->in, i);
                accept_payment(R, i);
                Rider_debug_print(*R);
                flag = 1;
                pthread_mutex_unlock(&(servers[i]->mutex));
                fflush(stdout);
                break;
            }
            pthread_mutex_unlock(&(servers[i]->mutex));
        }
    }
    Ri = &R;
    printf("\nInitiating payment of rider %d at server %d", R->in, R->Server_to_pay);
    do_payment(R);
    Rider_debug_print(*R);
    int pay_server_id = R->Server_to_pay;
    fflush(stdout);
    pthread_mutex_lock(&(servers[pay_server_id]->mutex));
    payment_over(R, pay_server_id);
    pthread_mutex_unlock(&(servers[pay_server_id]->mutex));
    printf("\nRider %d has finished his Payment at server %d", R->in, pay_server_id);
    Ri = &R;
    return;
}