//contains the global variables and the get details for these variables

#include "3_auxillary_functions.h"

int number_of_drivers;
int number_of_servers;
int number_of_riders;

void get_details() // get the value of the number of drivers, servers and riders
{
    printf("\nEnter the number of:\n");
    printf("\t Drivers: \t");
    scanf("%d", &number_of_drivers);
    printf("\t Servers: \t");
    scanf("%d", &number_of_servers);
    printf("\t Riders: \t");
    scanf("%d", &number_of_riders);
}
