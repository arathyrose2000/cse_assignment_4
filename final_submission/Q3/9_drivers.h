// contains all the functions related to drivers

#include "8_server_thread.h"

void AcceptRide(int type, Rider *R, int i)
{
    Rider_debug_print(*R);
    Driver_debug_print(*drivers[i]);
    if (type == 1)
    {
        drivers[i]->state = (R->type == PREMIER) ? onRidePremier : onRidePoolOne;
        drivers[i]->type = R->type;
        drivers[i]->no_of_passengers = 1;
        drivers[i]->Rider_in_cab[0] = R->in;
        R->state = inCab;
        R->Driver_of_cab = i;
    }
    if (type == 2)
    {
        drivers[i]->state = onRidePoolFull;
        drivers[i]->no_of_passengers = 2;
        drivers[i]->Rider_in_cab[1] = R->in;
        R->state = inCab;
        R->Driver_of_cab = i;
    }
    Rider_debug_print(*R);
    Driver_debug_print(*drivers[i]);
}

void OnRide(int RideTime)
{
    sleep(RideTime);
}

void EndRide(Rider **Ri, int cab_driver_id)
{
    Rider *R;
    R = *Ri;
    Rider_debug_print(*R);
    Driver_debug_print(*drivers[cab_driver_id]);
    R->state = wait_for_pay;
    R->Driver_of_cab = -1;
    if (R->type == PREMIER)
    {
        drivers[cab_driver_id]->state = waitState;
        drivers[cab_driver_id]->type = NO_RIDE;
        drivers[cab_driver_id]->no_of_passengers = 0;
        drivers[cab_driver_id]->Rider_in_cab[0] = -1;
    }
    else if (R->type == POOL)
    {
        if (drivers[cab_driver_id]->state == onRidePoolOne)
        {
            drivers[cab_driver_id]->state = waitState;
            drivers[cab_driver_id]->type = NO_RIDE;
            drivers[cab_driver_id]->no_of_passengers = 0;
            drivers[cab_driver_id]->Rider_in_cab[0] = -1;
        }
        else if (drivers[cab_driver_id]->state == onRidePoolFull)
        {
            drivers[cab_driver_id]->state = onRidePoolOne;
            drivers[cab_driver_id]->type = POOL;
            drivers[cab_driver_id]->no_of_passengers = 1;
            int other_passenger = (drivers[cab_driver_id]->Rider_in_cab[0] == R->in) ? drivers[cab_driver_id]->Rider_in_cab[1] : drivers[cab_driver_id]->Rider_in_cab[0];
            drivers[cab_driver_id]->Rider_in_cab[0] = other_passenger;
            drivers[cab_driver_id]->Rider_in_cab[1] = -1;
        }
    }
    Rider_debug_print(*R);
    Driver_debug_print(*drivers[cab_driver_id]);
    Ri = &R;
}