//contains all the function prototypes regarding the threads

#include "5_structure_defintions.h"

void *Rider_thread(void *arg);
int BookCab(Rider **);
void makePayment(Rider **Ri);
void Rider_create_thread(Rider *R);
void Rider_join_thread(Rider R);

void *Server_thread(void *arg);
void Server_join_thread(Server S);
void do_payment(Rider *R);
void accept_payment(Rider *R, int pay_server_id);
void payment_over(Rider *R, int pay_server_id);
void Server_create_thread(Server *S);

void AcceptRide(int type, Rider *R, int driver_no);
void OnRide(int RideTime);
void EndRide(Rider **R, int cab_driver_id);
