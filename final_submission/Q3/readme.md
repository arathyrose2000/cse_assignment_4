
# OBER CAB SERVICES

## QUESTION

Given N cabs, M riders and K payment servers you need to implement a working system which ensures correctness and idempotency. Each cab has one driver. In Ober, payments by the riders should be done in Payment Servers only. Ober provides two types of cab services namely pool and premier ride. In pool ride maximum of two riders can share a cab whereas in premier ride only one rider. There are four states for a cab namely waitState (no rider in cab), onRidePremier, onRidePoolFull (pool ride with two riders), onRidePoolOne (pool ride with only one rider).

## INPUT

Number of cabs, riders and servers

## LOGIC AND IMPLEMENTATION

### RIDERS

All the riders are considered as structures.
```
    int in;
    int state;
    pthread_t tid;
    int type;
    int arrivalTime;
    int maxWaitTime;
    int RideTime;
    int Driver_of_cab;
    int Server_to_pay;
```

At the start, all the riders are initialised. Then, we create one thread per rider.

#### Arrival

Each rider takes a random interval of time to arrive. It is taken care that no two riders come at the same time by using the auxillary function set_randomized() that create an array with n random numbers with no numbers repeated.

#### BookCab

Once the rider arrives, his state changes to waiting. He starts waiting for any free cab depending on the type of cab he wants to book. We use a variable flag to check if the rider has found a cab or not. As long as the flag is zero, we would keep checking the time elapsed and also keep on checking for more cabs.

To take the maximum wait time into consideration, the time taken from the time of arrival of the rider and the current time is calculated. If the time elapsed exceeds the maximum time, the rider quits the system.

If the rider needs a premier cab, he would require a driver in the waitState, while if he needs a pool cab, he would require a driver either in the waitState or onRidePoolOne.

When the rider finds a suitable driver, the mutex associated to the driver is locked, and acceptride() function is called. This function just changes the details of the driver to runnning. Then the mutex is unlocked.

Then onRide function is called that makes the thread sleep for the duration of the ride time.

Once the ride is done, the mutex associated to the driver is locked, and endride function is called. In this function, driver details are changed accordingly and then the mutex associated with the driver is unlocked. the state of the rider now becomes wait_for_pay.

#### makePayment

Then the rider searches for a free server to do payment. To check if it is free or not, the rider first locks the mutex associated with the server and checks the status of the server and then unlocks it. If the server is free, the mutex associated with the server is locked and accept_payment function is invoked that changes the details of the server. The payment is initiated. Then the mutex is unlocked.

Then do_payment function is invoked that makes the thread sleep for the payment procedure. Once that is done, the server becomes free, payment_over function is invoked that changes the rider status to payment_done. Then the rider leaves the system.

### DRIVERS(CABS)

All the drivers are considered as structures

```
    int in;
    int state;
    int type;
    int no_of_passengers;
    int Rider_in_cab[2];
    pthread_mutex_t mutex;
```

The drivers have the following functionalities:

#### void AcceptRide(int type, Rider *R, int i)

Changes the corresponding details of the rider and the driver, depending on the variable type. If type is one, this means that the driver was initially in his waitState. Otherwise, the  driver was in a single pool cab.

#### void OnRide(int RideTime)

Simulates the riding of the rider on the cab by sleeping for ridetime.

#### void EndRide(Rider **Ri, int cab_driver_id)

Changes the corresponding details of the rider and the driver once the ride is over.

### SERVERS

All the drivers are considered as structures

#### void accept_payment(Rider *R, int i)

Changes the rider and server details accordingly when the server accepts the rider for his payment

#### void do_payment(Rider *R)

Simulates the payment procedure of the rider by sleeping for 2 seconds that is, the payment time.

#### void payment_over(Rider *R, int pay_server_id)

Changes the rider and server details accordingly once the server is done with the payment procedure

### MAIN FUNCTION

First, we create arrays of rider pointers, driver pointers and server pointers and create the rider threads. We join the rider thread. Then we clear all the memory allocated for riders, drivers and servers.

## HOW TO RUN

> gcc -pthread Q3.c  
> ./a.out

## SAMPLE RUN

Enter the number of:
         Drivers:       2
         Servers:       3
         Riders:        18

SIMULATION BEGINS
  
Rider 11 has arrived requiring a cab of type POOL, ride time 16, and maximum wait time 9  
Driver 0 accepted the 11 rider for a pool ride  
Rider 6 has arrived requiring a cab of type POOL, ride time 19, and maximum wait time 10  
Driver 0 accepted the 6 rider for a pool ride with ride time 19  
Rider 9 has arrived requiring a cab of type POOL, ride time 10, and maximum wait time 6  
Driver 1 accepted the 9 rider for a pool ride  
Rider 16 has arrived requiring a cab of type PREMIER, ride time 16, and maximum wait time 10  
Rider 3 has arrived requiring a cab of type PREMIER, ride time 18, and maximum wait time 7  
Rider 5 has arrived requiring a cab of type POOL, ride time 13, and maximum wait time 10  
Driver 1 accepted the 5 rider for a pool ride with ride time 13  
Rider 15 has arrived requiring a cab of type POOL, ride time 17, and maximum wait time 8  
Rider 4 has arrived requiring a cab of type POOL, ride time 19, and maximum wait time 9  
Rider 14 has arrived requiring a cab of type POOL, ride time 13, and maximum wait time 10  
Rider 3 had a time Out. Hence he is exiting the system  
Driver 1 has completed his ride with 9 rider  
Rider 9 has completed his ride and is now waiting for payment  
Rider 9 has found a free server 0  
Initiating payment of rider 9 at server 0  
Driver 1 accepted the 15 rider for a pool ride with ride time 17  
Rider 0 has arrived requiring a cab of type PREMIER, ride time 20, and maximum wait time 7  
Driver 0 has completed his ride with 11 rider  
Rider 11 has completed his ride and is now waiting for payment  
Rider 11 has found a free server 1  
Initiating payment of rider 11 at server 1  
Driver 0 accepted the 14 rider for a pool ride with ride time 13  
Rider 17 has arrived requiring a cab of type POOL, ride time 10, and maximum wait time 8  
Rider 16 had a time Out. Hence he is exiting the system  
Rider 13 has arrived requiring a cab of type POOL, ride time 20, and maximum wait time 9  
Rider 9 has finished his Payment at server 0  
Rider 9 has left the system  
Rider 2 has arrived requiring a cab of type POOL, ride time 17, and maximum wait time 10  
Rider 11 has finished his Payment at server 1  
Rider 11 has left the system  
Rider 7 has arrived requiring a cab of type PREMIER, ride time 10, and maximum wait time 6  
Rider 4 had a time Out. Hence he is exiting the system  
Rider 8 has arrived requiring a cab of type POOL, ride time 19, and maximum wait time 6  
Driver 0 has completed his ride with 6 rider  
Rider 6 has completed his ride and is now waiting for payment  
Rider 6 has found a free server 0  
Initiating payment of rider 6 at server 0  
Driver 0 accepted the 8 rider for a pool ride with ride time 19  
Driver 1 has completed his ride with 5 rider  
Rider 5 has completed his ride and is now waiting for payment  
Rider 5 has found a free server 1  
Initiating payment of rider 5 at server 1  
Driver 1 accepted the 13 rider for a pool ride with ride time 20  
Rider 0 had a time Out. Hence he is exiting the system  
Rider 1 has arrived requiring a cab of type POOL, ride time 20, and maximum wait time 7  
Rider 10 has arrived requiring a cab of type POOL, ride time 16, and maximum wait time 5  
Rider 6 has finished his Payment at server 0  
Rider 6 has left the system  
Rider 5 has finished his Payment at server 1  
Rider 5 has left the system  
Rider 17 had a time Out. Hence he is exiting the system  
Rider 12 has arrived requiring a cab of type POOL, ride time 15, and maximum wait time 10  
Rider 7 had a time Out. Hence he is exiting the system  
Rider 2 had a time Out. Hence he is exiting the system  
Driver 0 has completed his ride with 14 rider  
Rider 14 has completed his ride and is now waiting for payment  
Rider 14 has found a free server 0  
Initiating payment of rider 14 at server 0  
Rider 10 had a time Out. Hence he is exiting the system  
Driver 0 accepted the 12 rider for a pool ride with ride time 15  
Rider 1 had a time Out. Hence he is exiting the system  
Rider 14 has finished his Payment at server 0  
Rider 14 has left the system  
Driver 1 has completed his ride with 15 rider  
Rider 15 has completed his ride and is now waiting for payment  
Rider 15 has found a free server 0  
Initiating payment of rider 15 at server 0  
Rider 15 has finished his Payment at server 0  
Rider 15 has left the system  
Driver 0 has completed his ride with 8 rider  
Rider 8 has completed his ride and is now waiting for payment  
Rider 8 has found a free server 0  
Initiating payment of rider 8 at server 0  
Driver 1 has completed his ride with 13 rider  
Rider 13 has completed his ride and is now waiting for payment  
Rider 13 has found a free server 1  
Initiating payment of rider 13 at server 1  
Rider 8 has finished his Payment at server 0  
Rider 8 has left the system  
Driver 0 has completed his ride with 12 rider  
Rider 12 has completed his ride and is now waiting for payment  
Rider 12 has found a free server 0  
Initiating payment of rider 12 at server 0  
Rider 13 has finished his Payment at server 1  
Rider 13 has left the system  
Rider 12 has finished his Payment at server 0  
Rider 12 has left the system  

SIMULATION ENDS

## IN CASE IT SHOWS A SEGMENTATIION FAULT, RUN THE CODE AGAIN
