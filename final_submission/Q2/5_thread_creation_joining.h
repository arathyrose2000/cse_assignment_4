//contains functions related to creating and joining threads and freeing memory of structure pointers

#include "4_thread_related_functions_prototypes.h"

void Cook_create_thread(Cook *C)
{
    pthread_create(&(C->tid), NULL, Cook_thread, (void *)C);
}
void Table_create_thread(Table *T)
{
    pthread_create(&(T->tid), NULL, Table_thread, (void *)T);
}
void Student_create_thread(Student *S)
{
    pthread_create(&(S->tid), NULL, Student_thread, (void *)S);
}
void Cook_join_thread(Cook *C)
{
    pthread_join(C->tid, NULL);
}
void Table_join_thread(Table *T)
{
    pthread_join(T->tid, NULL);
}
void Student_join_thread(Student *S)
{
    pthread_join(S->tid, NULL);
}
void Cook_delete(Cook *C)
{
    pthread_mutex_destroy(&C->mutex);
    free(C);
}
void Table_delete(Table *T)
{
    pthread_mutex_destroy(&T->mutex);
    free(T);
}
void Student_delete(Student *S)
{
    free(S);
}