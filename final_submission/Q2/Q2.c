#include "8_Student_thread.h"

int main()
{
    srand(time(0));
    get_input();
    printf("\n Simulation begins");
    // create all cooks and initialise them
    cooks = (Cook **)malloc((sizeof(Cook *)) * (number_of_cooks));
    for (int i = 0; i < number_of_cooks; i++)
    {
        cooks[i] = (Cook *)malloc(sizeof(Cook));
        Cook_init(cooks[i], i);
    }
    // create all tables and initialise them
    tables = (Table **)malloc((sizeof(Table *)) * (number_of_serving_tables));
    for (int i = 0; i < number_of_serving_tables; i++)
    {
        tables[i] = (Table *)malloc(sizeof(Table));
        Table_init(tables[i], i);
    }
    // create all students and initialise them
    students = (Student **)malloc((sizeof(Student *)) * (number_of_students));
    for (int i = 0; i < number_of_students; i++)
    {
        students[i] = (Student *)malloc(sizeof(Student));
        Student_init(students[i], i);
    }
    // create all the threads
    for (int i = 0; i < number_of_cooks; i++)
        Cook_create_thread(cooks[i]);
    for (int i = 0; i < number_of_students; i++)
        Student_create_thread(students[i]);
    for (int i = 0; i < number_of_serving_tables; i++)
        Table_create_thread(tables[i]);
    // join only the student thread (as we want the simulation to get over once all students are served)
    for (int i = 0; i < number_of_students; i++)
        Student_join_thread(students[i]);
    //freeing everything
    for (int i = 0; i < number_of_cooks; i++)
        Cook_delete(cooks[i]);
    free(cooks);
    for (int i = 0; i < number_of_serving_tables; i++)
        Table_delete(tables[i]);
    free(tables);
    for (int i = 0; i < number_of_students; i++)
        Student_delete(students[i]);
    free(students);
    printf("\n Simulation Over.");
    fflush(stdout);
}