//contains all the header files and the auxillary functions like generating a random number, finding the maximum and minimum of two numbers
//also contains the global variables related to the number of cooks, tables and students and the getvalue() function to get its value

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int random_integer(int minimum, int maximum) // generate a random number between minimum and maximum
{
    return random() % (maximum - minimum + 1) + minimum; //inclusive
}

int max(int a, int b) // take maximum of two integers
{
    return (a > b) ? a : b;
}
int min(int a, int b) // take minimum of two integers
{
    return (a < b) ? a : b;
}

int number_of_serving_tables;
int number_of_cooks;
int number_of_students;

void get_input() // get the number of cooks, serving tables, and tables
{
    printf("\n Enter the number of :\n");
    printf("\tRobot cooks:\t");
    scanf("%d", &number_of_cooks);
    printf("\tServing tables:\t");
    scanf("%d", &number_of_serving_tables);
    printf("\tStudents:\t");
    scanf("%d", &number_of_students);
}