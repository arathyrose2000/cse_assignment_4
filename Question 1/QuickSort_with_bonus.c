// Deals with quick sort with bonus (refer readme for more)

#include "Normal_quick_sort.h"

int main()
{
    srand(time(0));
    long long int n;
    printf("\nEnter the number of elements: ");
    scanf("%lld", &n);
    long long int a[10000];
    printf("\nEnter the array elements: ");
    for (long long int i = 0; i < n; i++)
        scanf("%lld", &a[i]);
    long double t1, t2, t3;
    t1 = run_concurrent_process_quicksort(n, a);
    t2 = run_concurrent_thread_quicksort(n, a);
    t3 = run_normal_quicksort(n, a);
    printf("\nNormal_quicksort ran");
    printf("\n\t --- %Lf times faster than concurrent_process_quicksort", t1 / t3);
    printf("\n\t --- %Lf times faster than concurrent_thread_quicksort\n", t2 / t3);
}
