// This file contains everything regarding concurrent quick sort using processes

#include "Quick_sort.h"

/**
 * Allocating the shared memory
 * */

long long int *shareMem(size_t size)
{
    key_t mem_key = IPC_PRIVATE;
    long long int shm_id = shmget(mem_key, size, IPC_CREAT | 0666);
    return (long long int *)shmat(shm_id, NULL, 0);
}

/**
 * Creates two separate processes responsible for carrying out the left and right quicksorts and then merge them
 * */

void concurrent_process_quicksort(long long int *arr, long long int low, long long int high)
{
    if (number_of_elements(low, high) <= 0)
        _exit(1);
    if (number_of_elements(low, high) < 5)
    {
        insertion_sort(arr, low, high);
        _exit(1);
    }
    long long int pi = partition(arr, low, high);
    int pid1 = fork();
    if (pid1 == 0)
    {
        concurrent_process_quicksort(arr, low, pi - 1); // sort the lower part
        _exit(1);
    }
    else
    {
        int pid2 = fork();
        if (pid2 == 0)
        {
            concurrent_process_quicksort(arr, pi + 1, high); // sort the upper part
            _exit(1);
        }
        else
        {
            int status; // wait for both of them to finish
            waitpid(pid1, &status, 0);
            waitpid(pid2, &status, 0);
        }
    }
}

long double run_concurrent_process_quicksort(long long int n, long long int a[])
{
    long double start_time, end_time;
    printf("\nRunning concurrent quicksort using processes...");
    long long int *arr = shareMem(sizeof(long long int) * (n + 1));
    for (long long int i = 0; i < n; i++)
        *(arr + i) = a[i];
    start_time = get_time();
    concurrent_process_quicksort(arr, 0, n - 1);
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", *(arr + i));
    end_time = get_time();
    printf("Time: %Lf", end_time - start_time);
    shmdt(arr);
    return end_time - start_time;
}
