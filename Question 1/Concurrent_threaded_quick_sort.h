// This file contains everything regarding concurrent quick sort using threads

#include "Concurrent_process_quick_sort.h"

struct packet
{
    long long int l;
    long long int r;
    long long int *arr;
};

void *concurrent_threaded_quicksort(void *a)
{
    struct packet *args = (struct packet *)a;
    long long int low = args->l;
    long long int high = args->r;
    long long int *arr = args->arr;
    if (number_of_elements(low, high) <= 0)
        return NULL;
    if (number_of_elements(low, high) < 5)
    {
        insertion_sort(arr, low, high);
        return NULL;
    }
    // partition the array
    long long int pi = partition(arr, low, high);
    // sort lower array:  (arr, low, pi - 1);
    struct packet a1;
    a1.l = low;
    a1.r = pi - 1;
    a1.arr = arr;
    pthread_t tid1;
    pthread_create(&tid1, NULL, concurrent_threaded_quicksort, &a1);
    // sort higher array:    (arr, pi + 1, high);
    struct packet a2;
    a2.l = pi + 1;
    a2.r = high;
    a2.arr = arr;
    pthread_t tid2;
    pthread_create(&tid2, NULL, concurrent_threaded_quicksort, &a2);
    // wait for the two parts to get sorted
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    return NULL;
}

long double run_concurrent_thread_quicksort(long long int n, long long int a[])
{
    long double start_time, end_time;
    printf("\nRunning concurrent quicksort using threads...");
    pthread_t tid;
    struct packet p;
    p.l = 0;
    p.r = n - 1;
    p.arr = a;
    start_time = get_time();
    pthread_create(&tid, NULL, concurrent_threaded_quicksort, &p);
    pthread_join(tid, NULL);
    printf("\nResulting array: ");
    for (long long int i = 0; i < n; i++)
        printf("%lld ", *(p.arr + i));
    end_time = get_time();
    printf("Time: %Lf", end_time - start_time);
    return end_time - start_time;
}