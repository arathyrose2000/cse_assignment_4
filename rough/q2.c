#include "random_functions.h"
#include "all_constants.h"
#include "structure_definitions.h"
#include "functions_that_never_change.h"

void *cook_thread(void *args)
{
    struct cook *c = (struct cook *)args;
    while (number_of_students_left_to_serve > 0)
    {
        printf("\n Cook %d : making biriyani", c->index);
        c->isbusy = 1;
        sleep_random(2, 5);
        int number_of_vessels_made = random_integer(1, 10);                    // r
        int number_of_students_each_vessel_can_serve = random_integer(25, 30); //p
        for (int i = 0; i < number_of_vessels_made; i++)
        {
            c->vessels[i] = (struct vessel *)malloc(sizeof(struct vessel *));
            c->vessels[i]->number_of_slots = number_of_students_each_vessel_can_serve;
        }
        printf("\n Cook %d : done cooking %d vessels, each capable of serving %d students", c->index, number_of_vessels_made);
        printf("\n Cook %d : waiting for all vessels to be loaded", c->index);
        //lock
        c->no_of_vessels_left_to_serve = number_of_vessels_made;
        //unlock
        // biriyani_ready();
        printf("\n Cook %d : All vessels loaded.", c->index);
        c->isbusy = 0;
    }
}
void *serving_table_thread(void *args)
{
    struct serving_table *t = (struct serving_table *)args;
    while (number_of_students_left_to_serve > 0)
    {
        printf("\n Serving table %d empty at the moment", t->index);
        number_of_students_left_to_serve--;
    }
}
void *student_thread(void *args)
{
    struct student *stu = (struct student *)args;
    printf("\n Student %d has arrived. now he needs food", stu->index);
    stu->current_state = WAITING;
    printf("\n Student %d waiting for food", stu->index);
    //stu->slot = wait_for_slot();
    stu->current_state = GOT_A_SLOT;
    printf("\n Student %d got a slot", stu->index);
    //student_in_slot();
    stu->current_state = DONE;
    printf("\n Student %d done with biriyani and is leaving", stu->index);
}
void initialisation()
{
    get_input();
    printf("\nNOW LETS BEGIN");
    number_of_students_left_to_serve = number_of_students;
    //initialising the cooks
    struct cook **cooks = (struct cook **)malloc((sizeof(struct cook *)) * (number_of_cooks));
    for (int i = 0; i < number_of_cooks; i++)
        cooks[i] = (struct cook *)malloc(sizeof(struct cook));
    for (int i = 0; i < number_of_cooks; i++)
    {
        cooks[i]->index = i;
        cooks[i]->isbusy = 0;
        cooks[i]->no_of_vessels_left_to_serve = 0;
        cooks[i]->vessels = (struct vessel **)malloc(MAX_NUMBER_OF_VESSELS_POSSIBLE_TO_MAKE_AT_A_TIME * sizeof(struct vessel *));
        pthread_mutex_init(&(cooks[i]->mutex), NULL);
    }
    for (int i = 0; i < number_of_cooks; i++)
        pthread_create(&(cooks[i]->tid), NULL, cook_thread, cooks[i]);
    // initialising the serving tables
    struct serving_table **serving_tables = (struct serving_table **)malloc((sizeof(struct serving_table *)) * (number_of_serving_tables));
    for (int i = 0; i < number_of_serving_tables; i++)
        serving_tables[i] = (struct serving_table *)malloc(sizeof(struct serving_table));
    for (int i = 0; i < number_of_serving_tables; i++)
    {
        serving_tables[i]->index = i;
        serving_tables[i]->number_of_slots_filled = 0;
        pthread_mutex_init(&(serving_tables[i]->mutex), NULL);
        pthread_cond_init(&(serving_tables[i]->cond), NULL);
    }
    for (int i = 0; i < number_of_serving_tables; i++)
        pthread_create(&(serving_tables[i]->tid), NULL, serving_table_thread, serving_tables[i]);
    // initialising the students
    struct student **students = (struct student **)malloc((sizeof(struct student *)) * (number_of_students));
    for (int i = 0; i < number_of_students; i++)
        students[i] = (struct student *)malloc(sizeof(struct student));
    for (int i = 0; i < number_of_students; i++)
    {
        students[i]->index = i;
        students[i]->current_state = NOT_ARRIVED;
    }
    for (int i = 0; i < number_of_students; i++)
        pthread_create(&(students[i]->tid), NULL, student_thread, students[i]);
    //joining all the threads
    for (int i = 0; i < number_of_cooks; i++)
        pthread_join(cooks[i]->tid, 0);
    for (int i = 0; i < number_of_serving_tables; i++)
        pthread_join(serving_tables[i]->tid, 0);
    for (int i = 0; i < number_of_students; i++)
        pthread_join(students[i]->tid, 0);
    //freeing everything
    for (int i = 0; i < number_of_cooks; i++)
        free(cooks[i]);
    free(cooks);
    for (int i = 0; i < number_of_serving_tables; i++)
        free(serving_tables[i]);
    free(serving_tables);
    for (int i = 0; i < number_of_students; i++)
        free(students[i]);
    free(students);
    printf("\nEVERYONE IS FED");
}

int main()
{
    initialisation();
}