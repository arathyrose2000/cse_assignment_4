struct cook
{
    int index;
    pthread_t tid;
    int isbusy;
    struct vessel **vessels;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    int no_of_vessels_left_to_serve;
};
struct vessel
{
    int number_of_slots;
    struct cook *made_by_cook;
};

struct student
{
    int index;
    pthread_t tid;
    int current_state;
    struct slot *sl;
};
struct slot
{
    int index;
    struct serving_table *s;
    struct student *stu;
};
struct serving_table
{
    int index;
    pthread_t tid;
    pthread_mutex_t mutex;
    pthread_cond_t cond;
    struct slot *sl[MAX_NUMBER_OF_SLOTS_PER_TABLE];
    int number_of_slots_filled;
    int mode;
};