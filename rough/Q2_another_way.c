#include "all_h.h"
#define NEW_VOTER 0
#define WAITING_VOTER 1
#define ASSIGNED_VOTER 2

struct VESSEL
{
    int idx;
    pthread_t vessel_thread_id;
    struct Cook *cook;
    int number_of_slots;
    int flag;
};
struct Table
{
    int idx;
    pthread_t table_thread_id;
    struct Cook *cook;
    struct VESSEL *vessel;
    int status;
};
struct Cook
{
    int idx;
    pthread_t cook_thread_id;
    int number_of_vessels;
    int max_slots_in_vessel;
    int number_of_tables;
    int finished_tables;
    pthread_mutex_t mutex;
    pthread_cond_t conditionvar_1;
    pthread_cond_t conditionvar_2;
    struct VESSEL **vessels;
    struct Table **tables;
};
void *thread_for_vessel(void *args)
{
    struct VESSEL *vessel = (struct VESSEL *)args;
    struct Cook *cook = vessel->cook;
    pthread_mutex_t *mutex_point = &(cook->mutex);
    pthread_cond_t *condvar1_point = &(cook->conditionvar_1);
    pthread_cond_t *condvar2_point = &(cook->conditionvar_2);
    int i = 0;
    int j = 0;
    int maxnum = 0;
    while (1)
    {
        pthread_mutex_lock(mutex_point);
        if (cook->finished_tables == cook->number_of_tables)
        {
            pthread_mutex_unlock(mutex_point);
            break;
        }
        pthread_mutex_unlock(mutex_point);
        maxnum = rand() % (cook->max_slots_in_vessel) + 1;
        pthread_mutex_lock(mutex_point);
        vessel->number_of_slots = maxnum;
        vessel->flag = 0;
        pthread_mutex_unlock(mutex_point);
        printf("In cook %d and vessel %d, %d slots are free.\n", cook->idx, vessel->idx, maxnum);
        j = 0;
        while (j < maxnum)
        {
            i = rand() % cook->number_of_tables;
            pthread_mutex_lock(mutex_point);
            if (cook->tables[i]->status == WAITING_VOTER)
            {
                cook->tables[i]->status = ASSIGNED_VOTER;
                cook->tables[i]->vessel = vessel;
                cook->finished_tables++;
                j++;
                printf("In cook %d and vessel %d, table %d has been alloted.\n", cook->idx, vessel->idx, i);
            }
            pthread_mutex_unlock(mutex_point);
            if (cook->finished_tables == cook->number_of_tables)
            {
                pthread_mutex_unlock(mutex_point);
                break;
            }
        }
        if (j == 0)
        {
            printf("In cook %d, vessel %d is closing.\n", cook->idx, vessel->idx);
            break;
        }
        /* vessel executing voting phase. */
        printf("In cook %d and vessel %d has started voting phase.\n", cook->idx, vessel->idx);
        pthread_mutex_lock(mutex_point);
        vessel->number_of_slots = j;
        vessel->flag = 1;
        pthread_cond_broadcast(condvar1_point);
        while (vessel->number_of_slots)
            pthread_cond_wait(condvar2_point, mutex_point);
        pthread_mutex_unlock(mutex_point);
        printf("In cook %d, vessel %d has finished voting phase.\n", cook->idx, vessel->idx);
    }
    return NULL;
}
void *thread_for_table(void *args)
{
    struct Table *table = (struct Table *)args;
    pthread_mutex_t *mutex_point = &(table->cook->mutex);
    pthread_cond_t *condvar1_point = &(table->cook->conditionvar_1);
    pthread_cond_t *condvar2_point = &(table->cook->conditionvar_2);
    pthread_mutex_lock(mutex_point);
    table->status = 1;
    while (table->status == 1)
        pthread_cond_wait(condvar1_point, mutex_point);
    pthread_mutex_unlock(mutex_point);
    struct VESSEL *vessel = table->vessel;
    pthread_mutex_lock(mutex_point);
    while (vessel->flag == 0)
        pthread_cond_wait(condvar1_point, mutex_point);
    vessel->number_of_slots--;
    printf("In cook number %d, vessel number is %d and the table number %d has casted its vote.\n", vessel->cook->idx, vessel->idx, table->idx);
    pthread_cond_broadcast(condvar2_point);
    pthread_mutex_unlock(mutex_point);
    return NULL;
}
void *thread_for_cook(void *args)
{
    struct Cook *cook = (struct Cook *)args;
    for (int i = 0; i < cook->number_of_vessels; i++)
    {
        cook->vessels[i] = (struct VESSEL *)malloc(sizeof(struct VESSEL));
        cook->vessels[i]->idx = i;
        cook->vessels[i]->cook = cook;
        cook->vessels[i]->number_of_slots = 0;
        cook->vessels[i]->flag = 0;
    }
    for (int i = 0; i < cook->number_of_tables; i++)
    {
        cook->tables[i] = (struct Table *)malloc(sizeof(struct Table));
        cook->tables[i]->idx = i;
        cook->tables[i]->cook = cook;
        cook->tables[i]->status = NEW_VOTER;
        cook->tables[i]->vessel = NULL;
    }
    /* vessels and tables threads start */
    for (int i = 0; i < cook->number_of_vessels; i++)
        pthread_create(&(cook->vessels[i]->vessel_thread_id), NULL, thread_for_vessel, cook->vessels[i]);
    for (int i = 0; i < cook->number_of_tables; i++)
        pthread_create(&(cook->tables[i]->table_thread_id), NULL, thread_for_table, cook->tables[i]);
    for (int i = 0; i < cook->number_of_vessels; i++)
        pthread_join(cook->vessels[i]->vessel_thread_id, 0);
    for (int i = 0; i < cook->number_of_tables; i++)
        pthread_join(cook->tables[i]->table_thread_id, 0);
    /* freeing memory */
    for (int i = 0; i < cook->number_of_vessels; i++)
        free(cook->vessels[i]);
    for (int i = 0; i < cook->number_of_tables; i++)
        free(cook->tables[i]);
    free(cook->vessels);
    free(cook->tables);
    printf("%d cook is signing off.\n", cook->idx);
    return NULL;
}
int main()
{
    int number_of_cooks,number_of_tables;
    /* --- input --- */
    // M-> number_of_cooks given
    scanf("%d %d", &number_of_cooks,&number_of_tables,);
    
    int number_of_vessels[105] = {0};
    int max_slots_in_vessel[105] = {0};
    int number_of_tables[105] = {0};
    for (int i = 0; i < number_of_cooks; i++)
    {
        scanf("%d %d %d", &number_of_tables[i], &number_of_vessels[i], &max_slots_in_vessel[i]);
    }
    printf("COOKING STARTS.\n");
    struct Cook **cooks = (struct Cook **)malloc((sizeof(struct Cook *)) * (number_of_cooks));
    for (int i = 0; i < number_of_cooks; i++)
        cooks[i] = (struct Cook *)malloc(sizeof(struct Cook));
    for (int i = 0; i < number_of_cooks; i++)
    {
        cooks[i]->idx = i;
        cooks[i]->number_of_vessels = number_of_vessels[i];
        cooks[i]->max_slots_in_vessel = max_slots_in_vessel[i];
        cooks[i]->number_of_tables = number_of_tables[i];
        cooks[i]->finished_tables = 0;
        cooks[i]->vessels = (struct VESSEL **)malloc(sizeof(struct VESSEL *) * number_of_vessels[i]);
        cooks[i]->tables = (struct Table **)malloc(sizeof(struct Table *) * number_of_tables[i]);
        pthread_mutex_init(&(cooks[i]->mutex), NULL);
        pthread_cond_init(&(cooks[i]->conditionvar_1), NULL);
        pthread_cond_init(&(cooks[i]->conditionvar_2), NULL);
    }
    for (int i = 0; i < number_of_cooks; i++)
        pthread_create(&(cooks[i]->cook_thread_id), NULL, thread_for_cook, cooks[i]);
    for (int i = 0; i < number_of_cooks; i++)
        pthread_join(cooks[i]->cook_thread_id, 0);
    for (int i = 0; i < number_of_cooks; i++)
        free(cooks[i]);
    free(cooks);
    printf("ELECTION COMPLETES.\n");
    return 0;
}
