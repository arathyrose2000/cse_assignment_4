#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#define MAX_CUSTOMERS 25 // The maximum number of customer threads.
void randwait(int secs)  // Generate a random number…
{
    sleep((int)((1 * secs) + 1));
}
sem_t waitingRoom; // Limits the # of customers allowed to enter the waiting room at one time.
sem_t barberChair; // ensures mutually exclusive access to the barber chair.
sem_t barberPillow; //  allow the barber to sleep until a customer arrives.
sem_t seatBelt; //  make the customer to wait until the barber is done cutting his/her hair.
int allDone = 0; // stop the barber thread when all customers have been serviced.
void *customer(void *number)
{
    int num = *(int *)number;
    printf("Customer %d leaving for barber shop.\n", num); //  Leave for the shop and take some random amount of time to arrive.
    randwait(2);
    printf("Customer %d arrived at barber shop.\n", num);
    sem_wait(&waitingRoom); // Wait for space to open up in the waiting room…
    printf("Customer %d entering waiting room.\n", num);
    sem_wait(&barberChair); // Wait for the barber chair to become free.
    sem_post(&waitingRoom); // The chair is free so give up your spot in the  waiting room.
    printf("Customer %d waking the barber.\n", num); // Wake up the barber…
    sem_post(&barberPillow);
    sem_wait(&seatBelt); // Wait for the barber to finish cutting your hair.
    sem_post(&barberChair); // Give up the chair.
    printf("Customer %d leaving barber shop.\n", num);
}
void *barber(void *junk)
{
    while (!allDone) // While there are still customers to be serviced…
    {
        printf("The barber is sleeping\n"); // Sleep until someone arrives and wakes you..
        sem_wait(&barberPillow);
        if (!allDone)
        {
            printf("The barber is cutting hair\n"); // Take a random amount of time to cut the customer’s hair.
            randwait(2);
            printf("The barber has finished cutting hair.\n");
            sem_post(&seatBelt); // Release the customer when done cutting…
        }
        else
            printf("The barber is going home for the day.\n");
    }
}
int main(int argc, char *argv[])
{
    pthread_t btid;
    pthread_t tid[MAX_CUSTOMERS];
    long RandSeed;
    int numCustomers, numChairs;
    int Number[MAX_CUSTOMERS];
    printf("Enter the number of Custmors : ");
    scanf("%d", &numCustomers);
    printf("Enter the number of Charis : ");
    scanf("%d", &numChairs);
    // Initialize the numbers array.
    for (int i = 0; i < MAX_CUSTOMERS; i++)
        Number[i] = i;
    // Initialize the semaphores with initial values…
    sem_init(&waitingRoom, 0, numChairs);
    sem_init(&barberChair, 0, 1);
    sem_init(&barberPillow, 0, 0);
    sem_init(&seatBelt, 0, 0);
    // Create the barber.
    pthread_create(&btid, NULL, barber, NULL);
    // Create the customers.
    for (int i = 0; i < numCustomers; i++)
    {
        pthread_create(&tid[i], NULL, customer, (void *)&Number[i]);
        sleep(1);
    }
    // Join each of the threads to wait for them to finish.
    for (int i = 0; i < numCustomers; i++)
    {
        pthread_join(tid[i], NULL);
        sleep(1);
    }
    // When all of the customers are finished, kill the barber thread.
    allDone = 1;
    sem_post(&barberPillow); // Wake the barber so he will exit.
    pthread_join(btid, NULL);
}
