#include "all_h.h"
#define NEW_VOTER 0
#define WAITING_VOTER 1
#define ASSIGNED_VOTER 2

struct EVM
{
    int idx;
    pthread_t evm_thread_id;
    struct Booth *booth;
    int number_of_slots;
    int flag;
};
struct Voter
{
    int idx;
    pthread_t voter_thread_id;
    struct Booth *booth;
    struct EVM *evm;
    int status;
};
struct Booth
{
    int idx;
    pthread_t booth_thread_id;
    int number_of_evms;
    int max_slots_in_evm;
    int number_of_voters;
    int finished_voters;
    pthread_mutex_t mutex;
    pthread_cond_t conditionvar_1;
    pthread_cond_t conditionvar_2;
    struct EVM **evms;
    struct Voter **voters;
};
void *thread_for_evm(void *args)
{
    struct EVM *evm = (struct EVM *)args;
    struct Booth *booth = evm->booth;
    pthread_mutex_t *mutex_point = &(booth->mutex);
    pthread_cond_t *condvar1_point = &(booth->conditionvar_1);
    pthread_cond_t *condvar2_point = &(booth->conditionvar_2);
    int i = 0;
    int j = 0;
    int maxnum = 0;
    while (1)
    {
        pthread_mutex_lock(mutex_point);
        if (booth->finished_voters == booth->number_of_voters)
        {
            pthread_mutex_unlock(mutex_point);
            break;
        }
        pthread_mutex_unlock(mutex_point);
        maxnum = rand() % (booth->max_slots_in_evm) + 1;
        pthread_mutex_lock(mutex_point);
        evm->number_of_slots = maxnum;
        evm->flag = 0;
        pthread_mutex_unlock(mutex_point);
        printf("In booth %d and evm %d, %d slots are free.\n", booth->idx, evm->idx, maxnum);
        j = 0;
        while (j < maxnum)
        {
            i = rand() % booth->number_of_voters;
            pthread_mutex_lock(mutex_point);
            if (booth->voters[i]->status == WAITING_VOTER)
            {
                booth->voters[i]->status = ASSIGNED_VOTER;
                booth->voters[i]->evm = evm;
                booth->finished_voters++;
                j++;
                printf("In booth %d and evm %d, voter %d has been alloted.\n", booth->idx, evm->idx, i);
            }
            pthread_mutex_unlock(mutex_point);
            if (booth->finished_voters == booth->number_of_voters)
            {
                pthread_mutex_unlock(mutex_point);
                break;
            }
        }
        if (j == 0)
        {
            printf("In booth %d, evm %d is closing.\n", booth->idx, evm->idx);
            break;
        }
        /* evm executing voting phase. */
        printf("In booth %d and evm %d has started voting phase.\n", booth->idx, evm->idx);
        pthread_mutex_lock(mutex_point);
        evm->number_of_slots = j;
        evm->flag = 1;
        pthread_cond_broadcast(condvar1_point);
        while (evm->number_of_slots)
            pthread_cond_wait(condvar2_point, mutex_point);
        pthread_mutex_unlock(mutex_point);
        printf("In booth %d, evm %d has finished voting phase.\n", booth->idx, evm->idx);
    }
    return NULL;
}
void *thread_for_voter(void *args)
{
    struct Voter *voter = (struct Voter *)args;
    pthread_mutex_t *mutex_point = &(voter->booth->mutex);
    pthread_cond_t *condvar1_point = &(voter->booth->conditionvar_1);
    pthread_cond_t *condvar2_point = &(voter->booth->conditionvar_2);
    pthread_mutex_lock(mutex_point);
    voter->status = 1;
    while (voter->status == 1)
        pthread_cond_wait(condvar1_point, mutex_point);
    pthread_mutex_unlock(mutex_point);
    struct EVM *evm = voter->evm;
    pthread_mutex_lock(mutex_point);
    while (evm->flag == 0)
        pthread_cond_wait(condvar1_point, mutex_point);
    evm->number_of_slots--;
    printf("In booth number %d, evm number is %d and the voter number %d has casted its vote.\n", evm->booth->idx, evm->idx, voter->idx);
    pthread_cond_broadcast(condvar2_point);
    pthread_mutex_unlock(mutex_point);
    return NULL;
}
void *thread_for_booth(void *args)
{
    struct Booth *booth = (struct Booth *)args;
    for (int i = 0; i < booth->number_of_evms; i++)
    {
        booth->evms[i] = (struct EVM *)malloc(sizeof(struct EVM));
        booth->evms[i]->idx = i;
        booth->evms[i]->booth = booth;
        booth->evms[i]->number_of_slots = 0;
        booth->evms[i]->flag = 0;
    }
    for (int i = 0; i < booth->number_of_voters; i++)
    {
        booth->voters[i] = (struct Voter *)malloc(sizeof(struct Voter));
        booth->voters[i]->idx = i;
        booth->voters[i]->booth = booth;
        booth->voters[i]->status = NEW_VOTER;
        booth->voters[i]->evm = NULL;
    }
    /* evms and voters threads start */
    for (int i = 0; i < booth->number_of_evms; i++)
        pthread_create(&(booth->evms[i]->evm_thread_id), NULL, thread_for_evm, booth->evms[i]);
    for (int i = 0; i < booth->number_of_voters; i++)
        pthread_create(&(booth->voters[i]->voter_thread_id), NULL, thread_for_voter, booth->voters[i]);
    for (int i = 0; i < booth->number_of_evms; i++)
        pthread_join(booth->evms[i]->evm_thread_id, 0);
    for (int i = 0; i < booth->number_of_voters; i++)
        pthread_join(booth->voters[i]->voter_thread_id, 0);
    /* freeing memory */
    for (int i = 0; i < booth->number_of_evms; i++)
        free(booth->evms[i]);
    for (int i = 0; i < booth->number_of_voters; i++)
        free(booth->voters[i]);
    free(booth->evms);
    free(booth->voters);
    printf("%d booth is signing off.\n", booth->idx);
    return NULL;
}
int main()
{
    int number_of_booths = 1;
    /* --- input --- */
    scanf("%d", &number_of_booths);
    int number_of_evms[105] = {0};
    int max_slots_in_evm[105] = {0};
    int number_of_voters[105] = {0};
    for (int i = 0; i < number_of_booths; i++)
        scanf("%d %d %d", &number_of_voters[i], &number_of_evms[i], &max_slots_in_evm[i]);
    printf("ELECTION STARTS.\n");
    struct Booth **booths = (struct Booth **)malloc((sizeof(struct Booth *)) * (number_of_booths));
    for (int i = 0; i < number_of_booths; i++)
        booths[i] = (struct Booth *)malloc(sizeof(struct Booth));
    for (int i = 0; i < number_of_booths; i++)
    {
        booths[i]->idx = i;
        booths[i]->number_of_evms = number_of_evms[i];
        booths[i]->max_slots_in_evm = max_slots_in_evm[i];
        booths[i]->number_of_voters = number_of_voters[i];
        booths[i]->finished_voters = 0;
        booths[i]->evms = (struct EVM **)malloc(sizeof(struct EVM *) * number_of_evms[i]);
        booths[i]->voters = (struct Voter **)malloc(sizeof(struct Voter *) * number_of_voters[i]);
        pthread_mutex_init(&(booths[i]->mutex), NULL);
        pthread_cond_init(&(booths[i]->conditionvar_1), NULL);
        pthread_cond_init(&(booths[i]->conditionvar_2), NULL);
    }
    for (int i = 0; i < number_of_booths; i++)
        pthread_create(&(booths[i]->booth_thread_id), NULL, thread_for_booth, booths[i]);
    for (int i = 0; i < number_of_booths; i++)
        pthread_join(booths[i]->booth_thread_id, 0);
    for (int i = 0; i < number_of_booths; i++)
        free(booths[i]);
    free(booths);
    printf("ELECTION COMPLETES.\n");
    return 0;
}
