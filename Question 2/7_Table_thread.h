//contains all the table related functions

#include "6_Cook_thread.h"

void *Table_thread(void *args) //gets invoked on creating a table thread
{
    Table *T = (Table *)args;
    while (there_are_more_students) //while(1)
    {
        debug_Table_details(*T);
        wait_for_vessel(T);
        printf("\n Serving table %d entering Serving Phase", T->i);
        ready_to_serve_table(T);
    }
}

void wait_for_vessel(Table *T) //returns once a vessel is transferred to it by a cook
{
    while (1)
    {
        pthread_mutex_lock(&T->mutex);
        if (T->status == TABLE_SERVING)
        {
            pthread_mutex_unlock(&T->mutex);
            return;
        }
        pthread_mutex_unlock(&T->mutex);
    }
}

void ready_to_serve_table(Table *T) //returns once all its slots are completely eaten by students
{
    while (1)
    {
        pthread_mutex_lock(&T->mutex);
        if (T->number_of_slots_left_in_vessel == 0 && T->number_of_slots_available == 0)
        {
            T->status = TABLE_WAITING;
            pthread_mutex_unlock(&T->mutex);
            printf("\n Serving Container of Table %d is empty, waiting for refill", T->i);
            return;
        }
        if (T->number_of_slots_available == 0)
        {
            T->number_of_slots_available = min(random_integer(1, 10), T->number_of_slots_left_in_vessel);
            T->number_of_slots_left_in_vessel = T->number_of_slots_left_in_vessel - T->number_of_slots_available;
            printf("\n Serving Table %d is ready to serve with %d slots", T->i, T->number_of_slots_available);
        }
        pthread_mutex_unlock(&T->mutex);
    }
}