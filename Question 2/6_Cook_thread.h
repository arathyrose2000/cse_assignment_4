//contains all the cook related functions

#include "5_thread_creation_joining.h"

void *Cook_thread(void *args) //gets invoked on creating a cook thread
{
    Cook *C = (Cook *)args;
    while (there_are_more_students)
    {
        debug_Cook_details(*C);
        prepare_biryani(C);
        printf("\n Robot Chef %d has prepared %d vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking", C->i, C->number_of_vessels_in_hand);
        biryani_ready(C);
        printf("\n All the vessels prepared by Robot Chef %d are emptied. Resuming cooking now.", C->i);
    }
}

void prepare_biryani(Cook *C) //prepare a random number of vessels of biryani and change his status to waiting
{
    C->status = COOK_COOKING;
    int k = random_integer(1, 10);
    printf("\n Robot Chef %d is preparing %d vessels of Biryani", C->i, k);
    sleep(random_integer(2, 5));
    C->number_of_vessels_in_hand = k;
    C->status = COOK_WAITING;
}

void biryani_ready(Cook *C) //waiting to transfer all the vessels made by him to the tables
{
    while (1)
    {
        for (int i = 0; i < number_of_serving_tables; i++)
        {
            if (C->number_of_vessels_in_hand == 0) //no more vessels so leave
                return;
            pthread_mutex_lock(&tables[i]->mutex); //book the table before making any changes
            if (tables[i]->status == TABLE_WAITING)
            {
                tables[i]->status = TABLE_SERVING;
                tables[i]->number_of_slots_left_in_vessel = C->number_of_slots_in_the_vessel_made;
                printf("\n Serving Container of Table %d is refilled by Robot Chef %d; Table %d resuming serving now", i, C->i, i);
                C->number_of_vessels_in_hand--;
            }
            pthread_mutex_unlock(&tables[i]->mutex); //unlock the table
        }
    }
}