//contains the function prototype of all the functions called while creating cooks, tables and students

#include "3_struct_def.h"

void Cook_create_thread(Cook *);
void Table_create_thread(Table *);
void Student_create_thread(Student *);
void Cook_join_thread(Cook *);
void Table_join_thread(Table *);
void Student_join_thread(Student *);

void *Cook_thread(void *);
void *Table_thread(void *);
void *Student_thread(void *);

void Cook_delete(Cook *);
void Table_delete(Table *);
void Student_delete(Student *);

void prepare_biryani(Cook *);
void biryani_ready(Cook *);

void wait_for_vessel(Table *);
void ready_to_serve_table(Table *);

void wait_for_slot(Student *);
void student_in_slot(Student *, int);