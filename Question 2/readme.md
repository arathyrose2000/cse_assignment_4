
# AUTOMATING BIRIYANI SERVING

## QUESTION

Automate the pipeline of serving Biryani in Kadamb mess

## INPUT

Number of robot cooks,serving tables and students

## LOGIC AND IMPLEMENTATION

### COOKS

All the cooks are considered as structures with the following structure

```
    int i;
    pthread_t tid;
    int status;
    int number_of_vessels_in_hand;
    int number_of_slots_in_the_vessel_made;
    pthread_mutex_t mutex;
```

At the start, the cooks are initialised with status COOK_WAITING. Then, we create one thread per cook. Initially the cook invokes prepare_biryani function where, the cook changes its state to COOK_COOKING and prepares a random number of vessels in a random time interval. Then its status changes to COOK_WAITING. Then it calls biryani ready function where it waits till it has transferred all the vessels made to the tables. To check the state of the table, we use mutex locks corresponding to that table. We access or modify the table value only after locking the mutex, and unlock it once we are done.

### TABLES

All the tables are considered as structures with the following structure

```
    int i;
    pthread_t tid;
    int number_of_slots_available;
    int number_of_slots_left_in_vessel;
    int status;
    pthread_mutex_t mutex;
```

Initially all the tables are initialised with state TABLE_WAITING and no slots available or left in the vessel. Create one thread per table that runs as long as there are more students left to serve (there is at least one student thread in progress). Here, we first wait for the vessel. This function wait_for_vessel() returns if and only if the table is in TABLE_SERVING state. Once the table has a vessel in its container, it invokes a ready_to_serve_table() function, which returns once all the slots in the vessel and those available are empty. It also takes care about making random number of slots available in case all the available slots were eaten up by the students. Here, we use table mutex to lock the table values while changing its details.

### STUDENTS

All the students are considered as structures with the following structure

```
    int i;
    pthread_t tid;
    int status;
    int arrival_time;
```

Initially all the students are initialised with state STUDENT_NOTARRIVED and with a random arrival times. Create one thread per student that runs until he is served. Initially sleep(arrival_time) to simulate the random arrival times of the students. Then once the student arrives, change his state to STUDENT_WAITINGFORSLOT and invoke the wait_for_slot function. Here we would check for random tables. If they have free slots then, the student occupies the slot and then invokes student_in_slot() function. The student gets served the biriyani almost instantaneously and they leave the system (The thread gets destroyed).

### MAIN FUNCTION

First, we create arrays of cook pointers, table pointers and student pointers and create the corresponding threads. We join only the student thread as we want the simulation to end once all the students are done. Then we clear all the memory allocated for cooks, students and tables.

## HOW TO RUN

> gcc -pthread main.c
> ./a.out

## SAMPLE RUN

 Enter the number of :  
        Robot cooks:    5  
        Serving tables: 5  
        Students:       10  
 Simulation begins  
 Robot Chef 1 is preparing 7 vessels of Biryani  
 Robot Chef 2 is preparing 6 vessels of Biryani  
 Robot Chef 3 is preparing 6 vessels of Biryani  
 Robot Chef 4 is preparing 3 vessels of Biryani  
 Student 0 has arrived  
 Student 0 is waiting to be allocated a slot on the serving table  
 Robot Chef 0 is preparing 10 vessels of Biryani  
 Student 9 has arrived  
 Student 9 is waiting to be allocated a slot on the serving table  
 Student 7 has arrived  
 Student 7 is waiting to be allocated a slot on the serving table  
 Robot Chef 3 has prepared 6 vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking  
 Serving Container of Table 0 is refilled by Robot Chef 3; Table 0 resuming serving now  
 Serving table 0 entering Serving Phase  
 Serving Table 0 is ready to serve with 9 slots  
 Serving Container of Table 1 is refilled by Robot Chef 3; Table 1 resuming serving now  
 Serving Container of Table 2 is refilled by Robot Chef 3; Table 2 resuming serving now  
 Serving table 1 entering Serving Phase  
 Serving Table 1 is ready to serve with 7 slots  
 Serving table 2 entering Serving Phase  
 Serving Table 2 is ready to serve with 5 slots  
 Student 7 assigned a slot on the serving table 2 and waiting to be served  
 Student 7 on serving table 2 has been served.  
 Serving Container of Table 3 is refilled by Robot Chef 3; Table 3 resuming serving now  
 Serving Container of Table 4 is refilled by Robot Chef 3; Table 4 resuming serving now  
 Student 0 assigned a slot on the serving table 1 and waiting to be served  
 Student 0 on serving table 1 has been served.  
 Serving table 3 entering Serving Phase  
 Serving Table 3 is ready to serve with 2 slots  
 Serving table 4 entering Serving Phase  
 Serving Table 4 is ready to serve with 7 slots  
 Student 9 assigned a slot on the serving table 0 and waiting to be served  
 Student 9 on serving table 0 has been served.  
 Robot Chef 2 has prepared 6 vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking  
 Robot Chef 0 has prepared 10 vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking  
 Robot Chef 1 has prepared 7 vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking  
 Robot Chef 4 has prepared 3 vessels of Biryani. Waiting for all the vessels to be emptied to resume cooking  
 Student 1 has arrived  
 Student 1 is waiting to be allocated a slot on the serving table  
 Student 1 assigned a slot on the serving table 4 and waiting to be served  
 Student 1 on serving table 4 has been served.  
 Student 5 has arrived  
 Student 5 is waiting to be allocated a slot on the serving table  
 Student 5 assigned a slot on the serving table 3 and waiting to be served  
 Student 5 on serving table 3 has been served.  
 Student 8 has arrived  
 Student 8 is waiting to be allocated a slot on the serving table  
 Student 8 assigned a slot on the serving table 4 and waiting to be served  
 Student 8 on serving table 4 has been served.  
 Student 4 has arrived  
 Student 4 is waiting to be allocated a slot on the serving table  
 Student 4 assigned a slot on the serving table 1 and waiting to be served  
 Student 4 on serving table 1 has been served.  
 Student 2 has arrived  
 Student 2 is waiting to be allocated a slot on the serving table  
 Student 2 assigned a slot on the serving table 4 and waiting to be served  
 Student 2 on serving table 4 has been served.  
 Student 3 has arrived  
 Student 3 is waiting to be allocated a slot on the serving table  
 Student 3 assigned a slot on the serving table 1 and waiting to be served  
 Student 3 on serving table 1 has been served.  
 Student 6 has arrived  
 Student 6 is waiting to be allocated a slot on the serving table  
 Student 6 assigned a slot on the serving table 1 and waiting to be served  
 Student 6 on serving table 1 has been served.  
 Simulation Over.
