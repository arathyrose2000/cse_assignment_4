// contains the structure definitions for cooks, students and tables; and their initialisation functions and display functions

#include "2_status_constants.h"

typedef struct
{
    int i;
    pthread_t tid;
    int status;
    int number_of_vessels_in_hand;
    int number_of_slots_in_the_vessel_made;
    pthread_mutex_t mutex;
} Cook;

typedef struct
{
    int i;
    pthread_t tid;
    int number_of_slots_available;
    int number_of_slots_left_in_vessel;
    int status;
    pthread_mutex_t mutex;
} Table;

typedef struct
{
    int i;
    pthread_t tid;
    int status;
    int arrival_time;
} Student;

void Cook_init(Cook *C, int i)
{
    C->i = i;
    C->status = COOK_WAITING;
    C->number_of_vessels_in_hand = 0;
    C->number_of_slots_in_the_vessel_made = random_integer(25, 50);
    pthread_mutex_init(&C->mutex, NULL);
}

void Table_init(Table *T, int i)
{
    T->i = i;
    T->status = TABLE_WAITING;
    T->number_of_slots_available = 0;
    T->number_of_slots_left_in_vessel = 0;
    pthread_mutex_init(&T->mutex, NULL);
}

void Student_init(Student *S, int i)
{
    S->i = i;
    S->status = STUDENT_NOTARRIVED;
    S->arrival_time = random_integer(0, number_of_students * 1.5);
}

void debug_Cook_details(Cook C)
{
    if (debug)
        printf("\n Cook %3d:   Status: %3d   Number_of_vessels: %3d   Number_of_slots_in_the_vessel_made: %3d", C.i, C.status, C.number_of_vessels_in_hand, C.number_of_slots_in_the_vessel_made);
}

void debug_Student_details(Student S)
{
    if (debug)
        printf("\n Student %3d:   Status: %3d   Arrival_time: %3d", S.i, S.status, S.arrival_time);
}

void debug_Table_details(Table T)
{
    if (debug)
        printf("\n Table %3d:   Status: %3d   Number_of_slots_available: %3d", T.i, T.status, T.number_of_slots_available);
}

Cook **cooks;
Table **tables;
Student **students;