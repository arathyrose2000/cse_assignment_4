//contains all the constants regarding statuses of cooks, tables and STUDENT_SERVED

#include "1_all_h.h"

//states of the cook
#define COOK_WAITING 0
#define COOK_COOKING 1

//states of the table
#define TABLE_WAITING 0
#define TABLE_SERVING 1

//states of the student
#define STUDENT_NOTARRIVED 0
#define STUDENT_WAITINGFORSLOT 1
#define STUDENT_INSLOT 2
#define STUDENT_SERVED 3

//debugging variable
#define debug 0
#define there_are_more_students 1
