//contains all the student related functions

#include "7_Table_thread.h"

void *Student_thread(void *args) //gets invoked on creating a student thread, and returns once the student has been served
{
    Student *S = (Student *)args;
    debug_Student_details(*S);
    sleep(S->arrival_time);
    printf("\n Student %d has arrived", S->i);
    S->status = STUDENT_WAITINGFORSLOT;
    printf("\n Student %d is waiting to be allocated a slot on the serving table", S->i);
    wait_for_slot(S);
    S->status = STUDENT_SERVED;
    return NULL;
}

void wait_for_slot(Student *S) //returns once the slot has been allocated
{
    while (1)
    {
        for (int i = random_integer(0, number_of_serving_tables - 1); i < number_of_serving_tables; i += random_integer(1, 2))
        {
            pthread_mutex_lock(&tables[i]->mutex);
            if (tables[i]->number_of_slots_available > 0)
            {
                tables[i]->number_of_slots_available--;
                pthread_mutex_unlock(&tables[i]->mutex);
                S->status = STUDENT_INSLOT;
                printf("\n Student %d assigned a slot on the serving table %d and waiting to be served", S->i, i);
                student_in_slot(S, i);
                S->status = STUDENT_SERVED;
                return;
            }
            pthread_mutex_unlock(&tables[i]->mutex);
        }
    }
}
void student_in_slot(Student *S, int j) //the student gets served biriyani here
{
    //  sleep(1);
    printf("\n Student %d on serving table %d has been served.", S->i, j);
    printf(" ");
}