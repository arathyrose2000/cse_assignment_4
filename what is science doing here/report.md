# SCIENCE 1 CLASS ASSIGNMENT

## Two drunks start out together at the origin, each having equal probability of making a step to the left or to the right along the x axis. Find the probability that they meet again after each one makes N steps. It is understood that they make steps simultaneously

### Computational Analysis

To find out the probability of both the drunks reaching the same position after N steps for a given N, run the following code

> python3 prob_n.py

Here, the ending position of the two drunks after n steps is calculated for a large number of times and compared, in order to calculate the probability of both the ending positions being the same.  
Input: Number of steps N  
Ouput: Probability that they meet each other after each one makes N steps

To plot a graph between the number of steps N and the corresponding probability, run the following code

> python3 prob_graph.py

Here, paths of t particles is calculated. For each pair in t, compare their positions at different value of N and increment the count accordingly. Then this function plots the graph between the current number of steps N and the probability of the particles being at the same position (count[N]/t*t)
