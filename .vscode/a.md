Commissioned Officers of the Indian Army

| RANK                                                                            | INSIGNIA                                                                      |
|---------------------------------------------------------------------------------|-------------------------------------------------------------------------------|
| Field Marshal                                                                   | National emblem over a crossed baton and saber in a lotus blossom wreath      |
| General                                                                         | National emblem over a five-pointed star, both over a crossed baton and saber |
| Lieutenant General                                                              | National emblem over crossed baton and saber                                  |
| Major General                                                                   | Five-pointed star over crossed baton and saber                                |
| Brigadier                                                                       | National emblem over three five-pointed stars in a triangular formation       |
| Colonel                                                                         | National emblem over two five-pointed stars                                   |
| Lieutenant Colonel                                                              | National emblem over five-pointed star                                        |
| Major                                                                           | National emblem                                                               |
| Captain                                                                         | Three five-pointed stars                                                      |
| Lieutenant                                                                      | Two five-pointed stars                                                        |
| missioned Officers of the Indian Army                                           | Gold national emblem with stripe                                              |
| Subedar (Infantry) or Risaldar (Cavalry and Armoured Regiments)                 | Two gold stars with stripe                                                    |
| Naib Subedar (Infantry) or Naib Risaldar (Cavalry and Armoured Regiments)       | One gold star with stripe                                                     |
| Havildar (Infantry) or Daffadar (Cavalry and Armoured Regiments)                | Three rank chevrons                                                           |
| Naik (Infantry) or Lance Daffadar (Cavalry and Armoured Regiments)              | Two rank chevrons                                                             |
| Lance Naik (Infantry) or Acting Lance Daffadar (Cavalry and Armoured Regiments) | One rank chevron                                                              |
| Sepoy                                                                           | Plain shoulder badge                                                          |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |
|                                                                                 | |e                                                                            |

LIST OF COMMANDS

